package it.vali;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

enum Fuel { GAS, DIESEL, PETROL, HYBRID, ELECTRIC
}


public class Car {


    private Person driver;
    private String person;
    private Person owner;
    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    private int maxPassengers;

    private List<Person> passengers = new ArrayList<Person>();

        // sulgudes klass ja muutuja
    public void addPassengers (Person passenger) {
                if(passengers.size() < maxPassengers) {
                    if(passengers.contains(passenger)) {
                        System.out.printf("Autos juba on reisija %s%n",passenger.getFirstName());
                        return;
                    }
            passengers.add(passenger);
                    System.out.printf("Autosse lisati reisija %s%n",passenger.getFirstName());
        } else {
            System.out.println("Rohkem inimesi autosse ei mahu");
        }

    }
    @Override
    public String toString() {
        return make + " " + model;
    }
    public void removePassenger(Person passenger) {
        if(passengers.indexOf(passenger) != -1) {
        passengers.remove(passenger);
            System.out.printf("Autost eemaldati reisija %s%n", passenger.getFirstName());
        } else {
            System.out.println("Autos sellist reisijat ei ole");
        }
    }

    public void showPassengers() {
        //Foreach loop- iga elemendi kohta listis passenger tekita objekt passengers
        // 1. kordus Person passenger on esimene reisija
        // 2. kordus Person passenger on teine reisija
        System.out.println("Autos on järgnevad reisijad:");
        for(Person passenger : passengers) {
            System.out.println(passenger.getFirstName());
        }
        // sama aga for i loop
//        for (int i = 0; i < passengers.size() ; i++) {
//            System.out.println(passengers.get(i).getFirstName());
//        }
    }

    public void maxPassengers() {
        this.maxPassengers = maxPassengers;

    }

    //kostruktor(constructor)
    //on eriline meetod, mis käivitatakse klassist objekti loomisel
    //saab vaikeväärtusi määrata
        public Car() {
            System.out.println("Loodi auto objekt");
            maxSpeed = 200;
            isUsed = true;
            fuel = Fuel.PETROL;
    }

    //construtor overloading
    public Car( String make, String model, int year, int maxSpeed, int maxPassengers) {
            this.make = make;
            this.model = model;
            this.year = year;
            this.maxSpeed = maxSpeed;
            this.maxPassengers = maxPassengers;
    }
    public Car( String make, String model, int year, Fuel fuel, boolean isUsed, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.fuel = fuel;
        this.isUsed = isUsed;
        this.maxSpeed = maxSpeed;

    }

    public void startEngine() {
        if(!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }

    public void stopEngine() {
        if(isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        } else {
            System.out.println("Mootor ei töötanudki");
        }
    }

    public void accelerate(int targetSpeed) {
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");

        } else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d%n", maxSpeed);
        } else if (targetSpeed < 0) {
            System.out.println("Auot kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada madalamale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", targetSpeed);
        }

    }
    // slowDown (int targetSpeed)
    public void slowDown (int targetSpeed) {
            if(!isEngineRunning) {
                System.out.println("Auto mootor ei tööta, ei saa aeglustada");
            } else if (targetSpeed < 0) {
                System.out.println("Auot kiirus ei saa olla negatiivne");

            }
            else if (targetSpeed > speed) {
                System.out.println("Auto ei saa aeglustada suuremale kiirusele");
            } else {
                speed = targetSpeed;
                System.out.printf("Auto on aeglustunud kiiruseni %d%n", targetSpeed);

            }

    }


    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    //park(), mis tegevused vaja teha( kutsu välja juba tehtud meetodid) aeglusta 0-ni ja mootor välja lülitada
    public  void park () {
            if(speed > 0) {
            System.out.println("Autot ei saa parkida varem kui auto on peatunud");
            slowDown(0);
            stopEngine();
                System.out.println("Auto pargiti");
        }

    }
    // lisa autole parameetrid driver ja owner(tüübist person) ja nendele siis get ja set meetod
    public Car( Person driver, Person owner) {
        this.driver = driver;
        this.owner = owner;

    }

    public String getMake() {
            return make;
    }
    public void setMake(String make) {
            this.make = make;
    }



    // loo mõni auto objekt, kellel on määratud kasutaja ja omanik

    //lisa autole võimalus hoida reisijaid( enne max reisijate arv)
    //lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // kontrolli, et ei lisataks rohkem reisijaid, kui mahub


}
