package it.vali;

enum Gender {
    FEMALE, MALE, NOTSPECIFIED
}

public class Person {
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;

    //kui klassil ei ole defineeritud konstruktorit, siis tehakse automaatselt nähtamatu
    //parameetriteta konstruktor, mille sisu on tühi
    // kui klassile ise lisada mingi konstruktor, siis nähtamatu kustutatakse
    // sellest klassist saab teha siis objekti ainult selle uue konstruktoriga

    public Person(String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Eesnimi on: %s, perekonnanimi on: %s, vanus on: %s%n",firstName, lastName, age);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int Age) {
        this.age = age;
    }

}
