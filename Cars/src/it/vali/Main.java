package it.vali;

import javax.crypto.spec.OAEPParameterSpec;

public class Main {

    public static void main(String[] args) {

        Car bmw = new Car();
        bmw.startEngine();
        bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();
        //
        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);

        Car fiat = new Car();
        Car mercedes = new Car();
        Car opel = new Car("Opel", "Vectra", 1999, 205, 5);
        opel.startEngine();
        opel.accelerate(210);

        Person person = new Person("Miki", "Hiir", Gender.NOTSPECIFIED, 25);
        Person otherPerson = new Person("Jaan", "Tatikas", Gender.MALE, 30);
        Person thirdPerson = new Person("Mari", "Maasikas", Gender.FEMALE, 20);

//        Car volvo = new Car("Miki", "Hiir", Gender.NOTSPECIFIED,25);
        opel.setDriver(person);
        opel.setOwner(person);
        System.out.printf("Sõiduki %s omaniku vanus on %d%n", opel.getMake(), opel.getOwner().getAge());

        opel.startEngine();
        opel.accelerate(120);
        opel.slowDown(0);
        opel.park();
        opel.addPassengers(person);
        opel.addPassengers(otherPerson);

        opel.showPassengers();
        opel.removePassenger(otherPerson);
        opel.addPassengers(thirdPerson);
        opel.showPassengers();
        opel.removePassenger(otherPerson);
        opel.showPassengers();
        Person juku = new Person("Juku", "Mets", Gender.MALE, 25);
        Person mari = new Person("Mari", "Maasikas", Gender.FEMALE, 20);

        System.out.println(juku);
        System.out.println(mari);
        System.out.println(opel);

    }
}




