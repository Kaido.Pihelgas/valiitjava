package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Split tükeldab stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
	    // split võimaldab tekitada massiivi. | = või
        String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";
        // splitib " " tühiku järgi või sisestatud märgi järgi, limit-mitmeks teeb
        String[] words = sentence.split(" ja | |, ");

        for (int i = 0; i < words.length ; i++) {
            System.out.println(words[i]);
        }
        System.out.println();

        String newSentence = String.join(" ", words);

        System.out.println(newSentence);

        newSentence = String.join(", ", words);

        System.out.println(newSentence);

        newSentence = String.join(" ja ", words);

        System.out.println(newSentence);
        // join liidab etteantud märgiga/elemendiga, nt andmebaasist sisse lugedes
        newSentence = String.join("\t", words);

        System.out.println(newSentence);
        //Escaping
        System.out.println("Juku\b ütles:\\n \"Mulle meeldib suvi\"");
        System.out.println("C:\\Users\\opilane\\Documents\\Vali-IT");

        //küsi rida numbreid nii, et ta paneb need kirja ühele reale eraldades tühikuga
        //seejärel liidab need küik kokku ja prindib vastuse

        System.out.println("Sisesta arvud, mida soovid liita, eralda need tühikuga");
        Scanner scanner = new Scanner(System.in);
        String numbersText = scanner.nextLine();
        String[] numbers = numbersText.split(" ");
        int summa = 0;

        for (int i = 0; i < numbers.length ; i++) {
            summa += Integer.parseInt(numbers[i]);

        }
        String joinedNumbers = String.join(", ", numbers);
        System.out.printf("Arvude %s summa on %d%n",joinedNumbers, summa);
    }
}
