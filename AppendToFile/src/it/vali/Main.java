package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
	//
        try {
            FileWriter fileWriter = new FileWriter("output.txt",true);
            // append true kirjutab üle ja lisab
            fileWriter.append("Tere\r\n");


            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    //1.koosta täisarvude massiiv 10 arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2
    int[] numbers =  new int[] {12, 34, 45, 1, 21, 33, 4, 6, 11, -2};

        try {
            FileWriter fileWriter = new FileWriter("Arrays count.txt");

            for (int i = 0; i < numbers.length; i++) {
                if(numbers[i] > 2) {
                    fileWriter.write(numbers[i] + System.lineSeparator());
                }
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Faili ei leitud.");

        }


    //2.Küsi kasutajalt 2 arvu,vv seni kuni mõlemad on korrektsed(on nr-ks teisendatavad)
    // ning nende summa pole paarisarv
    Scanner scanner = new Scanner(System.in);
    boolean correctNumber = false;
        int a = 0;
        int b = 0;

        do {
            try {
                System.out.println("Sisesta üks täisarv");
                a = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("See ei ole täisarv, proovi uuesti. ");
            }
        } while (!correctNumber);
        correctNumber = false;
        do {
            try {
                System.out.println("Sisesta veel üks täisarv");
                b = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("See ei ole täisarv, proovi uuesti. ");
            }
        } while (!correctNumber);

        int sumOdd = (a + b);
        if(sumOdd % 2 != 0) {
            System.out.println("Arvude summa pole paarisarv.");

        }else{
            System.out.println("Arvude summa on paarisarv");
        }




        //3. küsi mitu arvu ta tahab siseastada,seejärel küsi ühekaupa need arvud ning kirjuta
        // ning kirjuta nende arvude summa faili, nii et see lisatakse alati juurde (Append)
    // Tulemus on iga programmi käivitamisel järel on failis kõik eelnevad summad kirjas.
        System.out.println("Mitu arvu soovid sisestada?");
        int insertedCount = Integer.parseInt(scanner.nextLine());

        int sum = 0;
        for (int i = 0; i <insertedCount ; i++) {
            System.out.printf("Sisesta arv number %d%n", i + 1);
            int number = Integer.parseInt(scanner.nextLine());
            sum +=  number;

        }
        try {
            FileWriter fileWriter = new FileWriter("Arvude summa.txt",true);
            fileWriter.append("Sisestatud arvude summa on " + sum + System.lineSeparator());
            fileWriter.close();
            System.out.println("Vaata faili!");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //FileWriter fileWriter = new FileWriter("Arvude summa.txt");
        //fileWriter.write(scanner.nextLine());


        //fileWriter.close();
    }
}
