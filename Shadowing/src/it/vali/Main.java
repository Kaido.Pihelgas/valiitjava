package it.vali;

public class Main {

    static int a = 3;
    public static void main(String[] args) {
        int b = 7;
        System.out.println(b + a);
        // int a väärtus kehtib ainult { sees }
        // Main klassis olev a ei muutu!! e Shadowing
        a = 0;

        System.out.println(b + a);
        System.out.println(increasByA( 10));
        int a = 6;

        System.out.println(b + a);
        System.out.println(increasByA( 10));

         // kui vajadus muuta Mainis olevat a-d, siis klassi nimi. punkt ja muutuja a
        Main.a = 8;

        System.out.println(increasByA( 10));
        System.out.println(b + Main.a);
    }

    static int increasByA(int b) {
        return  b += a;

    }

}
