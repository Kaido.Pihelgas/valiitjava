https://www.w3schools.com/css/
stiliseerib HTML lehekülge, kuidas HTML-i elemente näidata.
väärtused pannakse tagide vahele= <style></style>
koosneb kahest osast:
body {
  background-color: lightblue;
}
body -- selektor/ tagi nimi- see kehtib kõigele, mis body tagide vahel on.
st otsi body tag ja pane kõik mis selle vahel on, nii nagu body kirjeldab.

body {
  background-color: #DC143C;
  color: blue;
}
värvikood #DC143C on 16-süsteemis (220,20,60) red value is 220, green value is 20 and the blue value of its RGB is 60
RGB koodis #000000- must #0000FF- sinine #00FF00- roheline #FF0000-punane

pilti edit paint-is, vali pipett ja Edit Colors, näitab koodi, see pane konverterisse rgb to hex 
(https://www.google.com/search?q=rgb+to+hex&rlz=1C1GCEV_enEE845EE845&oq=rgb&aqs=chrome.1.69i57j0l5.3167j0j4&sourceid=chrome&ie=UTF-8)
ja saad sama värvi kasutada


h1 {
  color: white;
  text-align: center;
  
}
h1 tähendab pealkiri
p {
  font-family: verdana;
  font-size: 20px;
  text-align: right;
}
see on teksti jaoks

<!DOCTYPE html>
<html>
<head>
<style>
#para1 {
  text-align: center;
  color: red;
}
</style>
</head>
<body>

<p id="para1">Hello World!</p>
<p>This paragraph is not affected by the style.</p>

</body>
</html>

#para1-- see on ainult määratletud rea jaoks,määratletakse <p id="para1"> mingi väärtus</p>

<!DOCTYPE html>
<html>
<head>
<style>
#para1 {
  text-align: center;
  color: red;
  background-color: yellow;
}

#para2 {
  text-align: right;
  font-size: 20px;
  background-color: lightgreen;
  color: blue;
}

p{
	color: white;
    background-color: black;
}
</style>
</head>
<body>

<p id="para1">Hello World!</p>
<p id="para2">This paragraph is not affected by the style.</p>
<p id="para3">Hello World!</p>


</body>
</html>
--ainult p{} määratleb kõigile aga eraldi toodud #para1 või #para2 on täpsemad ja jäävad kehtima.

.center {
  text-align: center;
  color: red;
}
.class selector kirjeldab ära, mis tehakse.
<!DOCTYPE html>
<html>
<head>
<style>
.center {
  text-align: center;
  color: red;
}
.left {
	text-align: left;
    color: blue;
    width: 100px;
}
</style>
</head>
<body>

<h1 class="center">Red and center-aligned heading</h1>
<p class="center">Red and center-aligned paragraph.</p> 
<button class="left">Nupp</button>
<p class="left">Blue and left-aligned paragraph.</p> 

</body>
</html>

<!DOCTYPE html>
<html>
<head>
<style>
p.center {
  text-align: center;
  color: red;
}
</style>
</head>
<body>

<h1 class="center">This heading will not be affected</h1>
<p class="center">This paragraph will be red and center-aligned.</p> 

</body>
</html>
--siin p.center klass, mis kehtib ainult neile, millel on täpselt sama kirjeldus ees p.center

<!DOCTYPE html>
<html>
<head>
<style>
h1, h2, p {
  text-align: center;
  color: red;
}
</style>
</head>
<body>

<h1>Hello World!</h1>
<h2>Smaller heading!</h2>
<p>This is a paragraph.</p>

</body>
</html>
--et kehtiks mitmele sama, saab reastada järjest ja eraldada komaga.
<!DOCTYPE html>
<html>
<head>
<style>
h1, h2, p,#miki {
  text-align: center;
  color: red;
}
.vasak {
  text-align: left;
  font-family: arial;
  color: blue;
}
</style>
</head>
<body>

<h1>Hello World!</h1>
<h2>Smaller heading!</h2>
<p>This is a paragraph.</p>
<p class= "vasak">Kolmas</p>
<input id="miki" type="text" value="Miki Hiir" />

</body>
</html>

Nagu js saab ka css kirjutada kolmel moel: External style sheet-- välisesse faili
Internal style sheet-- otse koodi
Inline style -- nupu sisse(kui vajutad buttonit, siis teeb midagi)

<!DOCTYPE html>
<html>
<body>

<h1 style="color:blue;margin-left:30px;">This is a heading</h1>
<p>This is a paragraph.</p>
<p style="color: green;">Roheline paragraph.</p>

</body>
</html>
--inline style.

<!DOCTYPE html>
<html>
<head>
<style>
div {
  border: 1px solid black;
  margin-top: 100px;
  margin-bottom: 100px;
  margin-right: 150px;
  margin-left: 80px;
  background-color: lightblue;
}
</style>
</head>
<body>

<h2>Using individual margin properties</h2>

<div>This div element has a top margin of 100px, a right margin of 150px, a bottom margin of 100px, and a left margin of 80px.</div><div>tere</div>

</body>
</html>
--div --teeb boksi, milles tekst muutub vastavalt akna suurusele,
margin- määrab ära, kui palju ruumi on enne ja pärast boksi/kasti 

<!DOCTYPE html>
<html>
<head>
<style>
div {
  border: 1px solid black;
  background-color: lightblue;
  padding-top: 50px;
  padding-right: 30px;
  padding-bottom: 50px;
  padding-left: 80px;
}
</style>
</head>
<body>

<h2>Using individual padding properties</h2>

<div>This div element has a top padding of 50px, a right padding of 30px, a bottom padding of 50px, and a left padding of 80px.</div>

</body>
</html>
-- padding lubab liigutada teksti elemendi sees e kus asub tekst boksis


