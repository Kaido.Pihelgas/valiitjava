package it.vali;

public class Main {

    public static void main(String[] args) {
	// kapseldamine ei lase muuta aga lubab lugeda
        // int vaikeväärtus 0
        // booleani vaikeväärtus false
        // double vaikeväärtus 0.0
        // stringi vaikeväärtus null
        // objektidel (Monitor, FileWriter) vaikeväärtus null
        // massiivi ( int[] arvud; ) vaikeväärtus null
        // float 0.0f

        Monitor firstMonitor = new Monitor();
        //saab välja kutsuda printimise läbi meetodi
        firstMonitor.setDiagonal(120);
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());
        System.out.println();

        firstMonitor.setManufacturer("Huawei");
        System.out.println();
        firstMonitor.setManufacturer("");
        firstMonitor.setDiagonal(27);

        firstMonitor.printInfo();


    }
}
