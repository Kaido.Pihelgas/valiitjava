package it.vali;

import java.util.Calendar;

//enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid,
// salvestatakse int-na, saab seostada a la String/double/int
//nt BLACK = 1, WHITE = 2 jne
//get/set on selleks, et saaks sekkuda ennem seadistamist/väärtuse andmist
enum Color {
        BLACK, WHITE, GREY
    }
enum ScreenType {
    LCD, TFT, OLED, AMOLED
    }

public class Monitor {
    private String manufacturer;
    private double diagonal; //tollides "
    private Color color;
    private ScreenType screenType;

    public int getYear() {
        return year;
    }

    private int year = 2000;


    public  double getDiagonal() {
        if(diagonal == 0) {
            System.out.println("Diagonaal on seadistamata");
        }
        return diagonal;
    }
    public void setDiagonal(double diagonal) {
        if(diagonal < 0) {
            System.out.println("Diagonaal ei saa olla negatiivne");
        } else if(diagonal > 100) {
            System.out.println("Diagonaal ei saa olla suurem kui 100\"");
        }
        else {
            //this tähistab seda konkreetset objekti
            this.diagonal = diagonal;
        }
    }
    // ära luba seadistada monitori tootjaks Huawei, kui keegi soovib seda tootjat, siis
    //prindi tootja puudub, keela ka tühja tootja nime lisamine
        //set/get lisada Code alt
    public String getManufacturer() {
        return manufacturer;
    }
    public void setManufacturer(String manufacturer) {
        if(manufacturer == null || manufacturer.equals("")) {
            this.manufacturer = "Tootja puudub";
        }
        else if(manufacturer.equals("Huawei")) {
            System.out.println("See tootja pole lubatud");
        }
        this.manufacturer = manufacturer;
    }

    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }
        //kui ekraanitüüp on sedaistamata(0), tagasta tüübiks LCD
    public ScreenType getScreenType () {
        if(screenType == null) {
            //tagastame vaikeväärtuse
           return ScreenType.LCD;
        }
        return screenType;
    }
    public void setScreenType(ScreenType screenType) {
        this.screenType = screenType;
    }

    public void printInfo() {
        System.out.println();
        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraanitüüp %s%n", getScreenType());
        System.out.println();
    }

    // tee meetod, mis tagastab ekraani diagonaali cm
    public double diagonalToCm() {
        return diagonal * 2.54;

    }
}
