package it.vali;

public class Main {

    public static void main(String[] args) {
	// leia üles esimene tühik, mis on tema asukoht
        //sümbolite indeksid tekstis algavad 0-st nagu massiivides
        // selleks indexOf

        String sentence ="kevadel elas metsas Mutionu keset kuuski noori vanu!";

        int spaceIndex = sentence.indexOf(" ");
        // kui tulem -1, siis seda otsitavatat pole
        // tagastab indeksi e kust otsitav sõna algab, kui fraas leitakse


        // kõigepelat otsime esimese tühiku ja siis sealt edasi
        // teine parameeter näitab, kust alustab otsimist
//        spaceIndex = sentence.indexOf(" ",spaceIndex + 1);
//        System.out.println(spaceIndex);

        // prindi kõigi tühikute indeksid


        while (spaceIndex != -1){
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ",spaceIndex + 1);

        }
        //tagantpoolt otsimine
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1){
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ",spaceIndex - 1);

        }

        // prindi lause esimesd neli tähte
        // substring prindib osa, (sulgudes indeksi asukohad)
        String part = sentence.substring(5, 21);

        System.out.println(part);

        //prindi lause esimene sõna
        String firstWord = sentence.substring(0, sentence.indexOf(" "));

        System.out.println(firstWord);

        // prindi lause teine sõna
        int spaceIndexTo = sentence.indexOf(" ");
        int secondSpace = sentence.indexOf(" ",spaceIndexTo + 1);

        String secondWord = sentence.substring(spaceIndexTo + 1, secondSpace);

        System.out.println(secondWord);

        // leia k-tähega algav sõna
        // leiaks ka siis , kui see sõna on ka lause esimana sõna
        int kIndex = sentence.indexOf(" k") + 1;
        int nextSpaceIndex = sentence.indexOf(" ",kIndex);
        String kWord = sentence.substring(kIndex, nextSpaceIndex);


        System.out.println(kWord);


        // leia k-tähega algav sõna
        String firstLetter = sentence.substring(0,1);
         kWord = "";



        if(firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0, spaceIndex);

        }else {
             kIndex = sentence.indexOf(" k") + 1;
            if(kIndex != 0) {
                spaceIndex = sentence.indexOf(" ", spaceIndex);
            }
        }
        if(kWord.equals("")) {
            System.out.println("K-tähega algavat sõna pole");
        }else {
            System.out.println("Esimene k-tähega algav sõna on " + kWord);
        }




        // leia mitu sõna on lauses

        int count = 0;
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1){

            spaceIndex = sentence.indexOf(" ",spaceIndex + 1);
            count++;

        }
        System.out.printf("Lauses on %d sõna.%n", count + 1);

        // leia mitu k-tähega algavat sõna on lauses
        int kCounter = 0;
        sentence = sentence.toLowerCase();

        int kAIndex = sentence.indexOf(" k");
        while (kAIndex != -1){

            kAIndex = sentence.indexOf(" k", kAIndex + 1);
            kCounter++;
        }
        if(sentence.substring(0, 1).equals("k")) {
            kCounter++;
        }
        System.out.printf("Lauses on %s k-tähega algavat sõna.%n", kCounter);

    }
}

