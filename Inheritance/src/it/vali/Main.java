package it.vali;

public class Main {

    public static void main(String[] args) {

        Cat angora = new Cat();
        angora.setName("Miisu");
        angora.setAge(10);
        angora.setBreed("Angoora");
        angora.setWeight(2.14);

        angora.printInfo();
        angora.eat();
        System.out.println();

        Cat persian = new Cat();
        persian.setName("Liisu");
        persian.setAge(1);
        persian.setBreed("Pärsia");
        persian.setWeight(1.6);

        persian.printInfo();
        persian.eat();
        System.out.println();

        Dog tax = new Dog();
        tax.setName("Muki");
        tax.setAge(4);
        tax.setBreed("Taksi");
        tax.setWeight(16);


        tax.printInfo();
        tax.eat();

        persian.catchMouse();
        tax.playWithCat(persian);


        //lisa pärinevusahelale puuduvad klassid
        //igale klassile 1 muutuja ja 1 meetod, mis on omane
        //wildAnimal /HerbivoreAnimal vs CarnivoreAnimal
        // DomesticAnimal / Pet vs FarmAnmial

    }
}
