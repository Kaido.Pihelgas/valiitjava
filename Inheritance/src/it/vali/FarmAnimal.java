package it.vali;

public class FarmAnimal extends DomesticAnimal {
    public boolean isLiveOnAFarm() {
        return liveOnAFarm;
    }

    public void setLiveOnAFarm(boolean liveOnAFarm) {
        this.liveOnAFarm = liveOnAFarm;
    }

    public boolean liveOnAFarm = true;
}
