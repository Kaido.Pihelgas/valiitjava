package it.vali;

public class Sheep extends  FarmAnimal {
    public boolean isGivesAWool() {
        return givesAWool;
    }

    public void setGivesAWool(boolean givesAWool) {
        this.givesAWool = givesAWool;
    }

    public  boolean givesAWool = true;

}
