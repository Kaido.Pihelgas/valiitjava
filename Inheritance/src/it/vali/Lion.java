package it.vali;

public class Lion extends CarnivoreAnimal {
    public boolean isAttacsByMultiple() {
        return attacsByMultiple;
    }

    public void setAttacsByMultiple(boolean attacsByMultiple) {
        this.attacsByMultiple = attacsByMultiple;
    }

    public boolean attacsByMultiple = true;

}
