package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
        //  saab teha lõpmatut tsüklit:
        //        while (true) {
        //            System.out.println("Tere");
        //        }
        // while tsükkel on tsükkel, mille korduste arv pole teada
        // valib ja küsib numbri
        // senikaua kuni kasutaja arvab valesti, ütleb proovi uuesti
        Scanner scanner = new Scanner(System.in);

        // random.nextInt genereerib numbri 0 - 4, liites + 1 , saame 1 - 5

        // 80-100
        //int number = random.nextInt(20) + 80;

        //sama tulemus:
//        int randomNum = ThreadLocalRandom.current().nextInt(1,6);


        do {
            Random random = new Random();
            int number = random.nextInt(8) + 1;
            System.out.println("Valisin juhuslikult ühe numbri, proovi see ära arvata");
            int answer = Integer.parseInt(scanner.nextLine());
            while (answer != number) {

                System.out.println("proovi uuesti!");
                answer = Integer.parseInt(scanner.nextLine());
            }


                System.out.println("Oled võitnud!");
                System.out.println("Kas soovid uuesti proovida? Jah/ei");

        }
        while (!scanner.nextLine().equals("ei"));
    }

    }





