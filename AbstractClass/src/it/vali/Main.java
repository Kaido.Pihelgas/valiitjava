package it.vali;

public class Main {

    public static void main(String[] args) {
	// abstraktne klass on hübriid liidesest ja klassist,
        //selles saab defineerida nii meetodi struktuure (nagu liidese)
        //aga saab ka defineerida meetodi koos sisuga
        // ei saa otse objekti luua, saab ainult pärineda


        ApartmentKitchen kitchen = new ApartmentKitchen();
        kitchen.setHeight(240);
        kitchen.becomeDirty();
        kitchen.setLength(400);


    }
}
