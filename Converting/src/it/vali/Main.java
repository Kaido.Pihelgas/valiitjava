package it.vali;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

	// küsi kasutajatelt 2 numbrit ja prindi nende summa
        System.out.println("Sisesta esimene number");
        int a = Integer.parseInt(scanner.nextLine());
        // nextLine tagastab alati stringi
        //parse-võtab sisu ja teeb stringist uue classi
        // Stringi jaoks teisendamine .valueOf
        System.out.println("Sisesta teine number");
        int b = Integer.parseInt(scanner.nextLine());
        int c = a + b;
        System.out.println("Esimese ja teise numbri summa on " + c);
        // või
        System.out.printf("Esimese %d ja teise numbri %d summa on %d\n", a, b, a + b);


        // küsi 2 reaalarvu ja prindi nende jagatis
        System.out.println("Sisesta kolmas number");
        double d = Double.parseDouble(scanner.nextLine());
        System.out.println("Sisesta neljas number");
        double e = Double.parseDouble(scanner.nextLine());
        System.out.printf("Numbrite %.1f ja %.2f jagatis on %.3f",d ,e, d / e);
        // %.2F näitab .2 mitu kohta on peale koma
        // kui tahan näha 0,56 asemel 0.56
        // siis kasutan USA local- formaati
        // String.format(locale.US, -- muudab US tähestikuks
        System.out.println(String.format(Locale.UK, "%.2f", c / d));
    }
}
