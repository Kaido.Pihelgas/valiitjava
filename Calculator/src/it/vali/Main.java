package it.vali;

public class Main {

    public static void main(String[] args) {
	   int sum = sum(4,5);
	   System.out.printf("Arvude summa on %d%n",sum);
	   System.out.printf("Arvude vahe on %d%n", subtract(8,12));
	   System.out.printf("Arvude korrutis on %d%n", multiply(5, 15));
	   System.out.printf("Arvude jagatis on %f%n", divide(23, 3));

       int[] numbers = new int[3];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = -2;
        sum = sum(numbers);

        System.out.printf("massiivi arvyde summa on %d%n", sum);
        numbers = new int[] {2, 5, 6, 12 ,-4};
        sum = sum(numbers);
        System.out.printf("massiivi arvude summa on %d%n", sum);

        //pööratud järjekorra esile kutsumine
        int[] reversed = reverseNumbers(numbers);
        printNumbers(reversed);

        printNumbers(new int[] {1, 2, 3, 4, 5});

        printNumbers(reverseNumbers(new int[] {1, 2, 3, 4, 5}));

        average(new int[] {1, 2, 3, 4, 5});
        System.out.println(average(new int[] {1, 2, 3, 4, 5}));

        printNumbers(convertToIntArray(new String[] {"2", "-12", "1", "0", "17"}));

        System.out.println();
        System.out.println(splitJoin(new String[] {"Juba", "linnukesed", "väljas", "laulavad"}));


        System.out.println();
        double radius = 10.25;
        System.out.printf("Ringi raadiusega %.2f ümbermõõt on %.2f%n",radius, circumference(radius) );

        int a = 23;
        int b = 80;
        System.out.printf("Arv %d moodustab arvust %d %.2f%%%n", a, b, percentage(a,b));
        //kaks %% märki tähistab % printimist

        System.out.printf("Massiivi arvude keskmine on %.2f%n", average(new int[]{3, 4, 6, 5}));

        System.out.printf("Sõnade massiiv liidetuna sõnadeks on %s%n",
                splitJoin(new String []{"kes", "elab", "metsa", "sees"}));

        System.out.printf("Sõnade massiiv liidetuna sõnadeks on %s%n",
                splitJoin(" ja ",new String []{"kes", "elab", "metsa", "sees"}));

    }
    // meetod, mis liidab 2 täisarvu kokku ja tagastab nenede summa
    // saab kohe kasutada
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }
    // lahuta/subtract
    static int subtract(int a, int b) {
        // võib otse kirjutada
        return a - b;

    }
    //korrutis-multiply
    static int multiply(int a, int b) {
        return a * b;
    }
    //jagatis- divide
    static double divide(int a, int b) {
        return (double)a / b;
    }
    //meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemendid kokku ning tagastab summa
    static int sum(int [] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length ; i++) {
            sum += numbers[i];
        }
        return sum;
    }
    //meetod, võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle
    //1 2 3 4 5
    //5 4 3 2 1
    static int [] reverseNumbers(int[] numbers) {

        int[] reversedNumbers = new int[numbers.length];//teeme uue massiivi, algse massiivi suurusega
        for (int i = 0; i < numbers.length ; i++) {//lisame mõlema massiivi loopi
            reversedNumbers[i] = numbers[numbers.length - i - 1];

        }
        return reversedNumbers;
    }
    //meetod prindib välja täisarvude massiivi elemendid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }



    //meetod, mis võtab parameetriks stringi massiivi (eeldusel, et tegelikult on seal numbrid, suudab teisendada
    //numbrite massiiviks ja tagastab selle
    static int[] convertToIntArray(String [] numbersAsText) {
        int [] numbers = new int [numbersAsText.length];//tehakse numbrite massiiv, mis on sama suur tekstimassiiviga
        for (int i = 0; i < numbersAsText.length ; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);//tõstab ühest massiivist teise ja samas teisendab numbriks

        }


        return numbers;
    }
    //meetod, mis võtab parameetriks Stringi masiivi ning tagastab lause, kus iga stringi vahel on tühik
    static String splitJoin(String [] sentence) {
        return String.join(" ", sentence);

        }



    //meetod, mis võtab parameetriks Stringi masiivi ning teine parameeter on
    //sõnade eraldaja. tagastada lause. eelmise ül tühiku asemel saad ise valida, mille pealt sõnu eraldad
    static String splitJoin(String delimiter, String [] sentence) {
        return String.join( delimiter, sentence);


    }

    //meetod, mis leiab numbrite massiivist keskmise ning tagastab selle
    //liidad kokku ja jagad liidetavate arvuga double
//    static double average(int[] numbers) {
//        int sum = 0;
//
//        for (int i = 0; i <numbers.length ; i++) {
//           sum += numbers[i];
//
//        }
//        int average = sum/ numbers.length;
//        return (double)average;
//    }
        //lihtsam variant
        static double average(int[] numbers) {
        return (double)sum(numbers) / numbers.length;
        }

    //meetod, mis arvutab mitu protsenti moodustab esimene arv teisest.
    static double percentage(int a, int b) {
        return a / (double)b *100;

    }

    //meetod, mis leiab ringi ümbermõõdu raadiuse järgi.
    static double circumference(double radius) {
        return  2 * Math.PI * radius;


    }
}

