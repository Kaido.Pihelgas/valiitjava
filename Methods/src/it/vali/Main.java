package it.vali;

import java.util.Scanner;

public class Main {
    // meetodid on mingid koodi osad, mis grupeerivad mingit teatud kindlat funktsionaalsust.
    //Kui koodis on korduvaid koodi osasid, on mõistlik teha meetod.


    public static void main(String[] args) {
        printHello();
        printHello(1);
        printText("Kuidas läheb?");
        printText("Hästi", 5);
        printText(2016, "seeneaasta");
        printText("tere", 1, true);
        printText("tere", 2, false);

    }

    //Lisame meetodi, mis prindib ekraanile Hello
    //mittenähtav staatiline ei tagasta meetod nimega printHello(parameetrit pole)
    private static void printHello() {
        System.out.println("Hello");
    }

    //lisame meetodi, mis prindib Hello ette antud kordi(selle saab määratleda parameeris)
    //kui pole kirjutatud public või private, siis on default private
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {

            System.out.println("Hello");
        }

    }

    //lisame meetodi, mis prindib etteaantud teksti
    //printText
    static void printText(String text) {

        System.out.println(text);

    }

    //lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi
    static void printText(String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);
        }

    }
    //method OVERLOADING- meil on mitu meetodit sama nimega, aga erineva parameetrite kombinatsiooniga
    //meetodi ülelaadimine


    static void printText(int year, String text) {

        System.out.printf("%d: %s%n", year, text);
    }

    //lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi
    // lisaks saab öelda, kas tahame kõiki suurtähti või mitte
    static void printText(String text, int howManyTimes, boolean toUpperCase) {
        for (int i = 0; i < howManyTimes; i++) {
            if (toUpperCase) {
                System.out.println(text.toUpperCase());
            } else {
                System.out.println(text);
            }

        }


    }
}


