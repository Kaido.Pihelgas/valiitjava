package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // generic array list on selline kollektsioon, kus
        // objekti loomise hetkel peame määrama, mis tüüpi elemente see sisaldama hakkab

        // tavaline List numbers = ArrayList();
        List<Integer> numbers = new ArrayList<>();

        numbers.add(10);
        numbers.add(1);
        numbers.add(2);

        List<String> words = new ArrayList<>();
        words.add("Tere");
        words.add("Head aega");



    }

}
