package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Country {

    private int population;
    private String name;
    private List<String> languages = new ArrayList<>();

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(String language) {
        languages.add(language);
    }

    @Override
    public String toString() {
        return String.valueOf(System.out.printf("Riigi info: %nNimi: %s%nRahvaarv: %s%nRiigis räägitakse keeli: %s" +
                "%n ", getName(), getPopulation(), getLanguages()));
    }




}
