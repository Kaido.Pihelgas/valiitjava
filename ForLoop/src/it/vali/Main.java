package it.vali;

public class Main {

    public static void main(String[] args) {
        // for tsükkel on selline, kus korduste arv on teada
        // koosneb 3-st komponendist
        // lõpmatu tsükkel
        //  for( ; ; ) {
        //  }
        // 1) int i = 0 => siin saab luua muutujaid ja neid algväärtustada
        // luuakse täisarv i, mille väärtus hakkab tsükli sees muutuma.
        // 2) i < 3 => see on tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks.
        // 3) i++ == i = i + 1 => see on tegevus, mida iga tsükli korduse lõpus korratakse.
        // i-- == i = i - 1

        for (int i = 0; i < 3; i++) {
            System.out.println("Väljas on ilus ilm");
        }

        //prindi ekraanile numbrid 1 -10
        for (int i = 0; i < 10; i++) {
            System.out.println(i + 1);
            //või

        }
        //paremini loetav!
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }
        //prindi numbrid 24-st 167ni

        for (int i = 24; i <= 167; i++) {
            System.out.println(i);
        }
        // prindi 18-st 3ni
        for (int i = 18; i >= 3; i--) {
            System.out.println(i);
            System.out.println();
        }
        // prindi numbrid 2,4,6,8,10
        // kahe võrra suurendamine i = i + 2 => i += 2
        // i = i -4 => i -= 4
        // iseenda korrutamine mingi arvuga i = i * 3 => i *= 3
        // i = i / 4 => i /= 4
        for (int i = 2; i <= 10; i += 2) {

            System.out.println(i);
        }
        System.out.println();
        // prindi 10 kuni 20 ja 40 kuni 60
        for (int i = 10; i <= 60; i++) {

            if (i <= 20 || i >= 40) {
                System.out.println(i);
            }
        }
        System.out.println();
        //või määratleme i väärtuseks 40
        for (int i = 10; i <= 60; i++) {

            if (i == 21) {
                i = 40;
            }
            System.out.println(i);
        }
        System.out.println();

        // prindi kõik arvus, mis jaguvad kolmega vahemikus 10 - 50
        // jäägi leidmine number a % 3
        // 4 % 3 => jääk 1
        // 6 % 3 => jääk 0
        // if (a % 3 == 0)
        for (int i = 10; i <= 50; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }
    }
}
