package it.vali;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
	//try plokis otsitakse/oodatakse Exceptionsit: kui kogu koodi plokis on exception, siis valib catch
        try {
            //FileWriter on selline klass, mis tegeleb faili kirjutamisega
            //sellest klassist objekti looomisel antakse talle ette faili asukoht
            //mis võib olla ainult faili nimega kirjeldatud(output.txt)
            // sel juhul kirjutatakse faili kausta, kus asub main.class või
            // täispika asukohaga c:\\users\\...

            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");
            // kirjutame mingi lause,System.lineSeparator()süsteemi põhine reavahetus, töötab ka Macil ja Linuxil
            fileWriter.write("Elas metsas Mutionu \r\nkeset kuuski noori vanu" + System.lineSeparator());
            fileWriter.write(String.format("Kadakmarja juure all, eluruum tal sügaval.%n"));
            //fileWriter pöördub op-süsteemi poole, et luua fail
            //vaja sulgeda fileWriteriga, muidu op-süsteem ei tea, et lõpetasid
            fileWriter.close();

            // catch plokis püütakse kinni kindlat tüüpi exception või kõik exceptionid, mis pärinevad
            //antud Exceptionist
        } catch (IOException e) {
            //printstacktrace = prinditakse välja meetodite välja kutsumise hierarhia/ajalugu
            //et mitte näidata lõppkasutajale, saab selle välja kommenteerida
            //e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs pole võimalik");
        }
    }
}
