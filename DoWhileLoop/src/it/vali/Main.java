package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
     // do While tsükkel on nagu tavaline while tsükkel aga
	// kontroll tehakse peale esimest tsüklit
    // üks kord tehakse kindlasti tsükkel
        // Tavaline while tsükkel:
//        System.out.println("Kas tahad jätkata? Jah/ei");
//        //jätkame seni, kuni kasutaja kirjutab "ei"
//           Scanner scanner = new Scanner(System.in);
//           String answer = scanner.nextLine();
//
//
//        while (!answer.equals("ei")){
//            System.out.println("Kas tahad jätkata? Jah/ei");
//            answer = scanner.nextLine();
//        }
        Scanner scanner = new Scanner(System.in);
        String answer;
        // iga muutuja, mille deklareerime, kehtib vaid teda ümbritsevate loogsulgude sees
        do {
            System.out.println("Kas tahad jätkata? Jah/ei");
             answer = scanner.nextLine();

        }
        while (!answer.equals("ei"));
    }
}
