package it.vali;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Map<String, String> map = new HashMap<String, String>();

        //Sõnaraamat(Map), vaja(saab) salvestada asjade paare, järjekorda ei salvesta(kui vaja,siis LinkedHashMap)
        //Maja => House
        //isa => dad
        //key => value
        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

        //oletame, et tahan teada, mis on inglise keeles "puu"
        String translation = map.get("Puu");
        System.out.println(translation);

        Map<String, String> idNumberName = new HashMap<String, String>();
        idNumberName.put("38502252725", "Jaan");
        idNumberName.put("48810122705", "Mari");
        idNumberName.put("38502252725", "Kalle");
        System.out.println(idNumberName.get("48502252725"));
        //kui kasutada .put sama key lisamisel, kirjutatakse value üle ja kehtima jääb viimane
        idNumberName.remove("38502252725");
        System.out.println(idNumberName.get("38502252725"));

        //EST => Estonia
        //Estonia => +372
        //loe lauses üle kõik erinevad tähed ning prindi välja iga tähe järel, mitu tk teda selles lauses oli
        //e-2
        // l-1
        //Char on selline tüüp, kus saab hoida üksikut sümbolit

        char symbolA = 'a';
        char symbolB = 'b';
        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();
        for (int i = 0; i < characters.length ; i++) {
            if(letterCounts.containsKey(characters[i])) {
                        letterCounts.put(characters[i], letterCounts.get(characters[i]) +1);
            }else {
                letterCounts.put(characters[i], 1);

            }
        }
        //Map.Entry<Character, Integer> on eraldi klass, mis hoiab ühte rida Mapis (Dictionarist)
        //e ühte key/ value paari,
        //entrySet- kontrollib ühte rida mapis
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda %n", entry.getKey(), entry.getValue());

        }
        System.out.println(letterCounts);

        //saab korraga kogu mapi paarid välja printida
        for (Map.Entry<String, String > entry :map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        Map<String, String> linkedMap = new LinkedHashMap<String, String>();

        //Sõnaraamat(Map), vaja(saab) salvestada asjade paare, järjekorda ei salvesta(kui vaja,siis LinkedHashMap)
        //Maja => House
        //isa => dad
        //key => value
        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

        for (Map.Entry<String, String > entry :map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
}
