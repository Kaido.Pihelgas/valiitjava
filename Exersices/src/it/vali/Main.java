package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.LongAccumulator;

public class Main {

    public static void main(String[] args) {
	// 1. arvuta ringi pindala, kui teada on raadius
        // prindi pindala välja S= 3,14 * r2

        double radius = 5.5;
        System.out.printf("Ringi raadiusega %.2f pindala on %.2f%n", radius, circleArea(radius));



        // 2. kirjuta meetod, mis tagastab boolean tüüpi väärtuse ja mille sisendparameetriteks on 2 stringi
        // Meetod tagastab kas tõene või vale selle kohta kas stringid on võrdsed


        Scanner scanner = new Scanner(System.in);

        System.out.println("Kirjuta üks sõna");
        boolean isSameString = false;

        String a = scanner.nextLine();
        System.out.println("Kirjuta veel üks sõna");

        String b = scanner.nextLine();
         if(a.equals(b)) {
             isSameString = true;
             System.out.println("Mõlemad sõnad on võrdsed");
             System.out.println(isSameString);
         } else {
             System.out.println("Ei ole võrdsed");
             System.out.println(isSameString);
         }



        // 3. kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi
        // iga sisendmassiivi elemendi kohta olgu tagastatavas massiivis sama palju a tähti
        // 3, 6 ,7 = aaa,aaaaaa, aaaaaaa
//        int[] numbers = new int[] {1, 2, 3, 4, 5};
//
//                 for (int i = 0; i < numbers.length; i++) {
//                     String aLetters = "";
//                     for (int j = 0; j < numbers[i]; j++) {
//                         aLetters += "a";
//                     }
//
//            System.out.println(aLetters);
//        }

        int[] numbers = new int[] {3, 10, 7};
         String[] words = intToStringA(numbers);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }







        // 4.kirjuta meetod, mis võtab sisendparameetrina aastaarvu, sisestada saab ainult aastaid vahemikus 500 - 2019
        //ütle veateade, kui ei mahu vahemikku)
        // ja tagastab kõik sellel sajandil esinenud liigaastad

        List<Integer> years = leapYearsInCentury(1400);

        for (int year : years) {
            System.out.println(year);
        }



        // 5. defineeri klass Language, sellel klassil setLanguageName, getLanguageName ning list riikide nimega,
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        // kõikide nimekirja eraldades komaga. Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning
        // prindi välja selle objekti toString() meetodi sisu.
        Language language = new Language();
        language.setLanguageName("Englich");

        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("UK");
        countryNames.add("Australia");
        countryNames.add("India");


        language.setCountryName(countryNames);

        System.out.println(language.toString());
        //sama lühemalt
        System.out.println(language);


    }
    static double circleArea(double radius) {
        return Math.PI * Math.pow(radius, 2);

    }
    // 2. kirjuta meetod, mis tagastab boolean tüüpi väärtuse ja mille sisendparameetriteks on 2 stringi
    // Meetod tagastab kas tõene või vale selle kohta kas stringid on võrdsed

    static boolean equals(String firstString, String secondString) {
        if(firstString.equals(secondString)) {
            return true;
        }
        return false;
    }

    // 3. kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi
    // iga sisendmassiivi elemendi kohta olgu tagastatavas massiivis sama palju a tähti
    // 3, 6 ,7 = aaa,aaaaaa, aaaaaaa
    static String[] intToStringA(int[] numbers) {
        String[] words = new String[numbers.length];
//        numbers[0] = 3;
//        numbers[1] = 7;
//        numbers[2] = 5;
        for (int i = 0; i < words.length; i++) {
            words[i] = generateAString(numbers[i]);
        }
        return words;
    }
    static String generateAString(int count) {
        String word = "";
        for (int i = 0; i < count ; i++) {
            word += "a";
        }
        return word;
    }

//    static void leapYears () {
//
//    }

    static List<Integer> leapYearsInCentury(int year) {
        List<Integer> years = new ArrayList<Integer>();

        if(year< 500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return years;
        }
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        for (int i = 2020; i >= centuryStart ; i -= 4) {
            if(i <= centuryEnd && i % 4 == 0) {
                years.add(i);
            }
        }
        return years;
    }





}
