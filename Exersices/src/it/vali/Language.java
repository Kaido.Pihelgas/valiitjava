package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Language {

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public List<String> getCountryName() {
        return countryName;
    }

    public void setCountryName(List<String> countryName) {
        this.countryName = countryName;
    }

    private String languageName;
    private List<String> countryName;

    @Override
    public String toString() {
        return String.join(", ",countryName);
    }
}

