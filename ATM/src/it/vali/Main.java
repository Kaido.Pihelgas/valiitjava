package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLOutput;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

    static String pin;

    public static void main(String[] args) {
        // Sisesta Pin kood, hoia failis pin.txt
        //Vali tegevus: a) sularaha sissemakse b) sularaha väljamakse c) kontojääk d) Muuda Pin koodi
        //valiti b)-
        //Vali summa: a) 5 EUR b) 10 EUR c) 20 EUR d) muu summa, kasutaja valib
        //Valiti d) - a) sisesta Pin kood b) sisesta uus Pin c)
        //kontojääk failis balance.txt
        // alustuseks küsi pin ja balance
        // lisa kontoväljavõtte funktsionaalsus
        // System.out.println(currentDateTimeToString() + "Sularaha väljavõtt - 100€");
        // salvestatud kõik tehingud


        int balance = loadBalance();
        pin = loadPin();

        if(!validatePin()) {
            System.out.println("Kaart konfiskeeritud!");
            return;//return lõpetab meetodi töö
        }
        //pin muutmine

        pin = "1234";
        savePin(pin);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta PIN kood");
        String answeredPin = scanner.nextLine();


        boolean wrongAnswer;
        do {

            do {
                System.out.println("Vali tegevus:");
                System.out.println("a) sularaha sissemakse");
                System.out.println("b) sularaha väljamakse");
                System.out.println("c) kontojääk");
                System.out.println("d) muuda PIN koodi");
                System.out.println("e) lõpetamine");

                String answer = scanner.nextLine();
                wrongAnswer = false;
                switch (answer) {
                    case "A":
                    case "a":
                        // SaveBalance();
                        System.out.printf("Sisesta sularaha%n");
                        int cashIn = Integer.parseInt(scanner.nextLine());
                        saveBalance((int) (balance + cashIn));
                        saveTransaction(cashIn, "Deposit");
                        System.out.printf("Sularaha sisestamine õnnestus. Teie kontojääk on %g%n", loadBalance());
                        System.out.println();
                        break;
                    case "B":
                    case "b":
                        System.out.printf("Vali summa:\n" +
                                "a) 5€\n + " +
                                "b) 10€\n + " +
                                "c) 20€\n + " +
                                "d) muu summa\n");
                        String cashOutAnswer = scanner.nextLine();
                        switch (cashOutAnswer) {
                            case "A":
                            case "a":
                                if (loadBalance() > 5) {
                                    balance -= 5;
                                    saveBalance((int) (balance));
                                    saveTransaction(cashOutAnswer, "Deposit");
                                    System.out.printf("Väljastatud 5€%n Teie kontojääk on %g%n", loadBalance());
                                    System.out.println();
                                }
                                if (loadBalance() < 5) {
                                    System.out.println("Kontol pole piisavalt vahendeid!");
                                    System.out.println();
                                }
                                break;
                            case "B":
                            case "b":
                                if (loadBalance() > 10) {
                                    balance -= 10;
                                    saveBalance((int) (balance));
                                    System.out.printf("Väljastatud 5€%n Teie kontojääk on %g%n", loadBalance());
                                    System.out.println();
                                }
                                if (loadBalance() < 10) {
                                    System.out.println("Kontol pole piisavalt vahendeid!");
                                    System.out.println();
                                }
                                break;
                            case "C":
                            case "c":
                                if (loadBalance() > 20) {
                                    balance -= 20;
                                    saveBalance((int) (balance));
                                    System.out.printf("Väljastatud 5€%n Teie kontojääk on %g%n", loadBalance());
                                    System.out.println();
                                }
                                if (loadBalance() < 20) {
                                    System.out.println("Kontol pole piisavalt vahendeid!");
                                    System.out.println();
                                }
                                break;
                            case "D":
                            case "d":
                                System.out.println("Sisestage soovitud summa.");
                                int cashOut = Integer.parseInt(scanner.nextLine());
                                if (loadBalance() > cashOut) {
                                    balance -= cashOut;
                                    saveBalance((int) (balance));
                                    System.out.printf("Väljastatud %d €%n Teie kontojääk on %g%n", cashOut, loadBalance());
                                    System.out.println();
                                }
                                if (loadBalance() < cashOut) {
                                    System.out.println("Kontol pole piisavalt vahendeid!");
                                    System.out.println();
                                }
                                break;
                        }
                        break;
                    case "C":
                    case "c":

                        System.out.printf("Kontojääk on %g", loadBalance());
                        break;
                    case "D":
                    case "d":
                        System.out.printf("Sisesta PIN kood\n");
                        scanner.nextLine();
                        System.out.println("Sisesta uus PIN kood");
                        scanner.nextLine();
                        System.out.println("Sisesta uus PIN kood");
                        scanner.nextLine();
                        savePin(scanner.nextLine());

                        loadPin();
                        break;
                    case "E":
                    case "e":
                        System.out.println("Head aega!");
                    default:
                        System.out.println("Selline tehing puudub");
                        wrongAnswer = true;
                        break;
                }

            }
            while (wrongAnswer);

            System.out.println("Kas tahad jätkata? j/e");
        }

         while (scanner.nextLine().equals("j"));



    //kontojäägi lisamine
        int amount = 100;
        balance -= 100;
        saveBalance(balance);
    }

    static String currentDateTimeToString() {
        //aja lisamine
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss EEEEE");

        Date date = new Date();
        return dateFormat.format(date);
    }

    static void savePin(String pin) {
        // tóimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }

    static String loadPin() {
        //toimub uue pin välja lugemine pin.txt failist
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi lugemine ebaõnnestus");
        }
        return pin;
    }

    static void saveBalance(int balance) {
        //toimub balance kirjutamine
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }
    }

    static int loadBalance() {
        double balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi lugemine ebaõnnestus");
        }
        return (int) balance;

    }



    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Sisesta PIN kood");
            String answeredPin = scanner.nextLine();

            if (answeredPin.equals(pin)) {
                //answeredPin = scanner.nextLine();
                System.out.println("Õige PIN");
                return true;
            }

        }
        return false;
        }
        static void saveTransaction(int amount, String transaction) {
        try {

            FileWriter fileWriter = new FileWriter("transaction.txt", true);


           if(transaction.equals("Deposit")) {
               fileWriter.append(String.format("%d Raha sissemaks +%d%n", currentDateTimeToString(),amount));

            }


         else {
                fileWriter.append(String.format("%d Raha väljamakse -%d%n", currentDateTimeToString(),amount));

            }
         fileWriter.close();
        }catch (IOException e) {
            System.out.println("Tehingu salvestamine ebaõnnestus");
        }
    }

}






