package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	//
//        for (int i = 1; i <= 5; i++) {
//            System.out.println(i);
//
//        }
        // sama while tsüklis

        int i = 1;

        while (i <= 5 ){
            System.out.println(i);
            i++;
        }
        // küsi mis päev täna on
        // seni kuni ta ära arvab


        Scanner scanner = new Scanner(System.in);

        // kohe saab kontrollida, kuna tühik "" on ju vale ja läheb tsüklisse
        String answer = "";
//        while (!answer.toLowerCase().equals("neljapäev")) {
//            System.out.println("Mis päev täna on?");
//            answer = scanner.nextLine();
//        }

        // for (int j = 0; j < ; j++) {
       // } jätame siit esimese ja viimase välja ära ja saame while tsükli
        for (;!answer.toLowerCase().equals("neljapäev");) {
        System.out.println("Mis päev täna on?");
        answer = scanner.nextLine();




        }



    }
}
