package it.vali;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEE");
        Calendar calendar = Calendar.getInstance();

//        //muuda päev eilseks
  //      calendar.add(Calendar.DATE, -1);
//
// sea muudetud päev aktuaalseks
       Date date = calendar.getTime();

      // System.out.println("Eilne päev oli: " + dateFormat.format(date));
        // muuda aasta
        calendar.add(Calendar.YEAR, 1);
        date = calendar.getTime();
        System.out.println("Täna aasta pärast on: " + dateFormat.format(date));

        //prindi selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad


        //selle aasta, selle kuu esimene kuupäev
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);

        int monthLeftThisYear = 11 - calendar.get(Calendar.MONTH);

        DateFormat weekDayFormat = new SimpleDateFormat("EEEE");

//        for (int i = 0; i < monthLeftThisYear; i++) {
//            calendar.add(Calendar.MONTH, 1);
//            date = calendar.getTime();
//            System.out.println(weekDayFormat.format(date));
//
//        }

        int currentYear = calendar.get(Calendar.YEAR);
        calendar.add(Calendar.MONTH, 1);
        while (calendar.get(Calendar.YEAR) == currentYear) {

            date = calendar.getTime();
            System.out.println(weekDayFormat.format(date));
            calendar.add(Calendar.MONTH, 1);
        }

    }
}
