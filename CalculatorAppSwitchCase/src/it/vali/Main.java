package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // küsi kaks arvu, seejärel küsi, mis tehet soovib teha
        //"vali tehe:a)liitmine, b)lahutamine, korrutamine, jagamine
        //prindi vastus
        Scanner scanner = new Scanner(System.in);
        boolean wrongAnswer;
        String answer;
        do {
            do {
                System.out.println("Sisesta üks arv:");
                int a = Integer.parseInt(scanner.nextLine());

                System.out.println("Sisesta teine arv:");
                int b = Integer.parseInt(scanner.nextLine());

                System.out.println("Millist tehet soovid teostada:\n a) liitmine\n b) lahutamine\n c) korrutamine\n d) jagamine");

                answer = scanner.nextLine();
                wrongAnswer = false;
//              if (answer.equals("a")) {
//                      System.out.printf("Arvude %d ja %d summa on %d%n", a, b, sum(a, b));
//                }
//                else if (answer.equals("b")) {
//                    System.out.printf("Arvude %s ja %s vahe on %d%n", a, b, subtract(a, b));
//                } else if (answer.equals("c")) {
//                    System.out.printf("Arvude %s ja %s korrutis on %d%n", a, b, multiply(a, b));
//                } else if (answer.equals("d")) {
//                    System.out.printf("Arvude %s ja %s jagatis on %.2f%n", a, b, divide(a, b));
//                } else {
//                    System.out.println("Sellist tehet pole valikus!");
//                    wrongAnswer = true;
//                }
                //switch/case konstruktsioon sobib kasutada if/else if asemel,
                // siis kui if/else if kontrollivad ühte ja sama muutuja väärtust
                //lõpeb käsuga default ja lõpeb alati break;
                switch (answer) {
                    // ilma breakita saab kasutada kohe ka suuri tähti
                    case "A":
                    case "a":
                        System.out.printf("Arvude %d ja %d summa on %d%n", a, b, sum(a, b));
                        break;
                    case "B":
                    case "b":
                        System.out.printf("Arvude %s ja %s vahe on %d%n", a, b, subtract(a, b));
                        break;
                    case "c":
                        System.out.printf("Arvude %s ja %s korrutis on %d%n", a, b, multiply(a, b));
                        break;
                    case "d":
                        System.out.printf("Arvude %s ja %s jagatis on %.2f%n", a, b, divide(a, b));
                        break;
                    default:
                        System.out.println("Sellist tehet pole valikus!");
                        wrongAnswer = true;
                        break;
                }
            }
            while (wrongAnswer) ;
            //raskem variant, must be(!answer.equals("a") && !answer.equals("b")
            //        && !answer.equals("c") && !answer.equals("d"));
            System.out.println("Kas tahad jätkata? Jah/ei");
            answer = scanner.nextLine();

        }
        while (!answer.equals("ei"));
    }


    static int sum ( int a, int b){
        int sum = a + b;
        return sum;
    }
    // lahuta/subtract
    static int subtract ( int a, int b){
        // võib otse kirjutada
        return a - b;

    }
    //korrutis-multiply
    static int multiply ( int a, int b){
        return a * b;
    }
    //jagatis- divide
    static double divide ( int a, int b){
        return (double)a / b;
    }

}
