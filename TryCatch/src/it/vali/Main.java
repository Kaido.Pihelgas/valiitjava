package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int a = 0;
        try {
//            String word = null;
//            word.length();
            int b = 4 / a;
        //kui on mitu catch plokki, siis otsib esimese ploki,
            // mille oskab antud Exception kinni püüda

        }catch (ArithmeticException e) {
            if(e.getMessage().equals("/ by zero")){
                System.out.println("Nulliga ei saa jagada!");
            }else{
            System.out.println("Esines aritmeetiline viga!");
            }
            // väike e on lühend exceptionist, saab välja tuua erinevaid meetodeid, saab kasutada arendusjärgus
            // vajalik logifailides
            //System.out.println(e.getMessage());
        }
        //see viga esineb programmi töötamise ajal
        catch (RuntimeException e) {
            System.out.println("Programmi töötamisel esines viga");
        }
        //Exception on klass, millest kõik erinevad exceptioni tüübid pärinevad,
        // mis omakorda tähendab, et püüdes kinni üldise exc. püüame kinni kõik exc.-d
        catch (Exception e) {
           System.out.println("Üldine viga");
        }
        //küsi number ja kui number ei ole korrektne siis ütleme mingi veateate
        // ja kordame küsimust kuni saame õige vastuse
        Scanner scanner = new Scanner(System.in);
        boolean correctNumber = false;
        do {
            System.out.println("Sisesta number");
            try {
                int number = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("Viga! See ei ole number. Proovi uuesti");

            }

        } while (!correctNumber);
            System.out.println("Tubli!");
        //loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        //näita vaeteadet
         int[] numbers = new int[] {1, 2, 3, 4, 5};
        try {
            numbers[5] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Viga! Siin pole nii palju ruumi!");
        }


    }


}
