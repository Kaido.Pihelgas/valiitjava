package it.vali;

public class Main {

    public static void main(String[] args) {
	// method overriding e meetodi ülekirjutamine, tähendab seda,
        // et kuskil klassis, millest antud klass pärineb
        //oleva meetodi sisu kirjutatakse pärinevas klassis üle
        //
        Dog buldog = new Dog();
        buldog.eat();
        buldog.setBreed("Bulldog");

        Cat siam = new Cat();
        siam.eat();
        siam.setBreed("Siiam");
        siam.setOwner("Marju");
        siam.printInfo();

        //kirjuta koera getAge üle nii, et kui koeral vanust on null, siis näitaks ikka 1

        System.out.printf("%s vanus on %d%n", buldog.getBreed(), buldog.getAge());

        //metsloomadel printinfo võiks kirjutada Nimi: metsloomal pole nime

        Lion lion = new Lion();
        Fox fox = new Fox();
        Antilopes gasell = new Antilopes();

        fox.setBreed("Hõberebane");
        fox.setAge(3);
        fox.setWeight(15);
        gasell.setBreed("Antiloop");
        gasell.setWeight(40);
        gasell.printInfo();
        fox.printInfo();
        lion.printInfo();
    }
}
