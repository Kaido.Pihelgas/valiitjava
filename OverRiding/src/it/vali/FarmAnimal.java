package it.vali;

public class FarmAnimal extends DomesticAnimal {
    public boolean isLiveOnAFarm() {
        return liveOnAFarm;
    }

    public void setLiveOnAFarm(boolean liveOnAFarm) {
        this.liveOnAFarm = liveOnAFarm;
    }

    public boolean liveOnAFarm = true;

    public String getFarmerName() {
        return farmerName;
    }

    public void setFarmerName(String farmerName) {
        this.farmerName = farmerName;
    }

    public String farmerName;



}
