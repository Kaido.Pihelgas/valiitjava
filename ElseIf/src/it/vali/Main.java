package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // küsi üks arv
        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta üks arv");
        int number = Integer.parseInt(scanner.nextLine());

        //kui nr on suurem 3-st, prindi
        if (number > 3) {
            System.out.println("Number on suurem kui 3");

        } else if (number == 0) {
            System.out.println("Number on null");
        } else if (number < 3) {
            System.out.println("Number on väiksem kui 3");

        } else {
            System.out.println("Numbrid on võrdsed");
        }
    }

}
