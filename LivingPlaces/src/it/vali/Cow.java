package it.vali;

public class Cow extends FarmAnimal {
    public boolean isGivesMilk() {
        return givesMilk;
    }

    public void setGivesMilk(boolean givesMilk) {
        this.givesMilk = givesMilk;
    }

    public boolean givesMilk = true;


}
