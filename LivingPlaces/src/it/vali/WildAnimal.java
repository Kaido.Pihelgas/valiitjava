package it.vali;

public class WildAnimal extends Animal {
    public boolean isEatsOtherAnimals() {
        return eatsOtherAnimals;
    }

    public void setEatsOtherAnimals(boolean eatsOtherAnimals) {
        this.eatsOtherAnimals = eatsOtherAnimals;
    }

    public boolean eatsOtherAnimals = true;

    @Override
    public String getName() {
        return "metsloomadel pole nime";
    }
}
