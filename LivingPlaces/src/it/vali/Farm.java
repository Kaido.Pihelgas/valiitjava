package it.vali;

import com.sun.jdi.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace{
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();
    //palju meil farmis loomi on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();
    // palju farmi loomi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    //
    public Farm() {
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Pig", 5);
        maxAnimalCounts.put("Sheep", 15);
    }

    @Override
    public void addAnimal(Animal animal) {
        //kas animal ei ole tüübist Farmanimal või pärineb sellest tüübist
        if(!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmiloomad");
            return;
            // sellepärast eitus ja return, et kood oleks lühem
        }
        //getSimpleName prindib ainult nime
        String animalType = animal.getClass().getSimpleName();



        //alustuseks uurime, kas kohta jagub(maxAnimalCounts-s) just seda tüüpi loomadele(animalType)
        if(!maxAnimalCounts.containsKey(animalType))
        {
            System.out.printf("Farmis loomadele %s kohta pole%n", animalType);
            return;
        }



        //uurime, kas seda looma on farmis
        if(animalCounts.containsKey(animalType)){
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount) {//kas on kohta
                System.out.printf("Farmis on loomale %s kõik kohad juba täis%n", animalType);
                return;
            }

            //lisame, enne kontrollime kuhu ja kas seal on seda tüüpi
            animalCounts.put(animalType, animalCounts.get(animalType)+ 1);
        } else {
            //sellist looma veel ei ole farmis, lisame
            animalCounts.put(animalType, 1);
        }

        animals.add((FarmAnimal) animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);


       // System.out.println(animalType);
    }
    // tee meetod, mis prindib välja kõik farmis elavad loomad ja mitu neid on
    @Override
    public void printAnimalCounts() {

        for (Map.Entry<String, Integer > entry : animalCounts.entrySet()) {
            System.out.printf("Farmis on %d looma %s%n", entry.getValue(), entry.getKey());
        }

    }

    // tee meetod, mis eemaldab farmist looma
    @Override
    public void removeAnimal(String animalType) {
        //teen muutuja, mis ma tahan, et iga element listist oleks : list, mida tahan läbi vaadata
        //FarmAnimal animal hakkab olema järjest esimene loom,
        // siis teine loom, kolmas ja seni kuni loomi on
        boolean animalFound = false;
        for (FarmAnimal farmAnimal :animals) {
            //käi kõik klassid läbi, leia nimi ja kas võrdub
            if(farmAnimal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(farmAnimal);
                System.out.printf("Farmist eemaldati loom %s%n", animalType);

                //kui see on viimane loom, siis eemalda see rida animalCounts mapist
                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                } else {
                    //muuljuhul vähenda animalCounts
                    animalCounts.put(animalType, animalCounts.get(animalType)- 1);
                }
                animalFound = true;
                break;

            }

        }
        // täienda, et kui ei leitud sellest tüübist looma, siis prindi
        //"Farmis selline loom puudub"-- boolean animalFound
        if(!animalFound) {
             System.out.printf("Farmis loom %s puudub%n", animalType);

        }
        //sama fori tsükliga
//        for (int i = 0; i <animals.size() ; i++) {
//            if(animals.get(i).getClass().getSimpleName().equals(animalType)) {
//                animals.remove(animals.get(i));
//            }
//        }


    }
}
