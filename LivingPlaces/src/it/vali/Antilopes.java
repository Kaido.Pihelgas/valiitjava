package it.vali;

public class Antilopes extends HerbivoreAnimal {
    public boolean isJumpsHigh() {
        return jumpsHigh;
    }

    public void setJumpsHigh(boolean jumpsHigh) {
        this.jumpsHigh = jumpsHigh;
    }

    public boolean jumpsHigh = true;
}
