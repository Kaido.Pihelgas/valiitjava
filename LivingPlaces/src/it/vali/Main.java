package it.vali;

public class Main {

    public static void main(String[] args) {
	// write your code here
        LivingPlace livingPlace = new Zoo();
        Sheep sheep = new Sheep();

        Pig pig = new Pig();
        pig.setName("Kalle");
        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal(new Horse());
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(pig);
        Cow cow = new Cow();
        livingPlace.addAnimal(cow);
        cow.setName("Maasu");
        livingPlace.removeAnimal("Pig");
        livingPlace.removeAnimal("Pig");
        livingPlace.addAnimal(sheep);
        livingPlace.removeAnimal("Cow");
        livingPlace.removeAnimal("Cow");
        livingPlace.addAnimal(sheep);

        livingPlace.printAnimalCounts();


        //mõelge j atäiendage Zoo/ Forest klassi, et neil oleks nende 3 meetodi sisud

    }
}
