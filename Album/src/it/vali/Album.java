//package com.hellokoding.account.model;

package it.vali;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Album {

    private String albumtitle;
    private int id;
    private List<Album> albums = new ArrayList<Album>();
    private String description;
    private Date albumCreated;
    private Map<String, Integer> likesCounts = new HashMap<String, Integer>();

    public String getAlbumtitle() {
        return albumtitle;
    }

    public void setAlbumtitle(String albumtitle) {
        this.albumtitle = albumtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public static void createAlbum() {

    }

    public static void deleteAlbum() {

    }

    public static void movePictureToAnotherAlbum() {


    }
    // tee meetod, mis prindib välja kõik farmis elavad loomad ja mitu neid on

    public void printLikesCounts() {

        for (Map.Entry<String, Integer > entry : likesCounts.entrySet()) {
            System.out.printf("Farmis on %d looma %s%n", entry.getValue(), entry.getKey());
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    public Map<String, Integer> getLikesCounts() {
        return likesCounts;
    }

    public void setLikesCounts(Map<String, Integer> likesCounts) {
        this.likesCounts = likesCounts;
    }

    public Date getAlbumCreated() {
        return albumCreated;
    }

    public void setAlbumCreated(Date albumCreated) {
        this.albumCreated = albumCreated;
    }



}