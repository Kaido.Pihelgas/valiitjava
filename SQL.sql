--See on lihtne teksti päring, mis tagastab ühe rea ja veeru
--(veerul puudub pealkiri). Selles veerus ja reas saab olema tekst Hello World
SELECT 'Hello World';
SELECT 3;
--*/kommentaar täisarvu saab küsida ilma märkideta ''
SELECT 'raud'+'tee';
-- ei tööta PostgreSQL´is, MSSql´is töötab
SELECT CONCAT ('all','maa','raud','tee', 'jaam','akorraldaja', 55,0);-- nii saab kokku liita
--standard CONCAT toimib kõigis SQL serverites
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'
-- , komaga lisab veerud
-- AS lisab veergudele pealkirjad, toimib ka ilma.
SELECT
	'Peeter' AS Eesnimi,
	'Paat' AS Perenimi,
	23 AS vanus,
	75.45 AS kaal,
	'Blond' AS juustevärv,
	SELECT NOW() kuupäev;
SELECT NOW() kuupäev;-- hetke kellaaeg,mingis vaikimisi formaadis
--kui tahan konkreetset osa, nt aastat või kuud, siis
SELECT date_part('day', NOW())
SELECT to_char(NOW(),'HH24:mi:ss DD.MM.YYYY')--kuupäeva formaatimine eesti kuupäeva formaati.

--Interval laseb lisada või eemaldada mingit ajaühikut
SELECT now()+ interval '1 day ago';
SELECT now() - interval '1 day';
SELECT now() + interval '2 centuries 3 years 2 months 1 weeks 3 days 5 minutes';
--tabeli loomine
CREATE TABLE student(
	id serial PRIMARY KEY,-- serial tähendab, et tüübiks on int,
	--mis hakkab 1 võrra suurenema, Primary Key(primaarvõti) on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, --ei tohi tühi olla
	last_name varchar(64) NOT NULL,
	height int NULL, -- tohib tühi olla
	weight numeric(5,2) NULL,
	birthday date NULL
);
SELECT * FROM student;--tabelist kõikide ridade küsimine
-- * tähendab, et anna kõik veerud
--saab ka täpsustada, mida nimelt teada tahetakse
SELECT 
	first_name AS eesnimi, 
	last_name AS perekonnanimi,
	--kuidas saada vanuse?
	 age(now(), birthday) AS vanus,
	 date_part ('year',now()) - date_part('year',birthday) AS vanus
FROM 
	student;
SELECT	date_part ('month', TIMESTAMP '2001-01-01') --kui mingit kindlat ajaühikut
SELECT CONCAT (first_name, ' ', date_part('year', birthday) ,' ' ,last_name) AS täisnimi FROM student;
--kui vaja filtreerida, siis WHERE
--filtreerime, kõik tudengid, kelle pikkus on 180 cm
SELECT 
*
FROM 
	student
WHERE
	height = 180
	-- küsi tabelist eesnime ja perekonnanime järgi mingi Peeter ja Mari
	SELECT
*
FROM
	student
WHERE
	(first_name = 'Peeter' AND last_name = 'Puujalg')
	OR 
	(first_name = 'Mari'	AND last_name = 'Maasikas')
	
-- anna kasutajad, kelle pikkus jääb 170-180 vahele
SELECT
*
FROM
	student
WHERE
	height <= 180 AND height >= 170
	
--anna kasutajad, kes on pikemad kui 170 ja lühemad kui 150
SELECT
*
FROM
	student
WHERE
	height < 170 OR height <150
	
-- anna kasutajate eesnimi ja pikkus, kelle sünnipäev on jaanuaris
SELECT
	first_name, height
FROM
	student 
	
WHERE
	date_part ('month', birthday) = 01
	
--anna kasutajad, kelle middle_name on null(määratlemata)
SELECT
	*
FROM
	student 
	
WHERE
	middle_name IS NULL--kui ei ole tühi(middle_name IS NOT NULL)
	
--anna kasutajad, kelle pikkus ei ole 180
SELECT
	*
FROM
	student 
	
WHERE
	height != 180 -- töötab ka(<>180)

--anna kasutajad, kelle pikkus on 175, 169, 149
SELECT
	*
FROM
	student 
	
WHERE
	--height = 175 OR height = 149 OR height = 169
	height IN (175, 169, 149)
	
--anna kasutajad, kelle eesnimi on Mari, Kalle või Peeter
SELECT
	*
FROM
	student 
WHERE
	first_name IN ('Kalle','Mari', 'Peeter')

--eelmine EI OLE
SELECT
	*
FROM
	student 
WHERE
	first_name NOT IN ('Kalle','Mari', 'Peeter')
	
--anna kasutajad, kelle sünnikuupäev on kuu esimene, neljas või viieteistkümnes päev
SELECT
	*
FROM
	student 
WHERE
	date_part('day', birthday) IN (1, 4, 15) 

--kõikidest WHERE võrdlustest jäävad välja NULL väärtusega väljad!
SELECT
	*
FROM
	student 
WHERE
	height > 0 OR height <= 0 OR height IS NULL -- vajalik eraldi määratleda
	height !=180

--pane pikkuse järjekorda väiksemast suuremaks, kui pikkused on võrdsed, järjesta kaalu järgi
SELECT
	*
FROM
	student 
ORDER BY
	height, weight DESC
	--kui tagurpidi , siis lisa DESC (descending)
	
--anna vanuse järjekorras vanemast nooremaks, pikkused, mis jäävad 160 ja 170 vahele
SELECT
	height	

FROM
	student 
WHERE
	height >= 160 AND height <=170
ORDER BY
	birthday
	
--kui vaja leida mingit ühendit või tähe, siis a% %ss% või %l
SELECT
	*
FROM
	student 
WHERE
	first_name LIKE 'P%'--algab
	OR first_name LIKE '%ee%'--sisaldab
	OR last_name LIKE '%ts'--lõpeb
	
--tabelisse uute andmete lisamine
INSERT INTO student
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Tiit', 'Tihane', 191, 115.4, '1975-04-07', NULL),
	('Mait','Mehine', 182,87.1, '22-07-1986', 'Maidu'),
	('Tiina','Kase', 178, 67.1, '24-10-1996', 'Tiiu'),
	('Marta','Mehine', 162, 61, '17-08-1997', NULL),
	('Jaan','Jaaniste', 172, 85.4, '02-07-1999', NULL);
	
--muutmiseks UPDATE, peab olema tähelepanelik, et kõiki väärtusi ei muudaks!
--alati peab kasutama WHERE-i lause lõpus
UPDATE
	student
SET
	height = 176
WHERE 
	first_name = 'Mati' AND last_name LIKE 'Kokk'
	
--muuda kõigi õpilaste pikkus ühe võrra suuremaks
UPDATE
	student
SET
	height = height + 1
	
--suurenda hiljem kui 1999 sündinutel sünnipäeva ühe päeva võrra
UPDATE
	student
SET
	birthday = birthday + interval '1 day'
WHERE
 	date_part('year', birthday) > 1999
	
---kustutamine sama moodi, alati kasuta DELETE FROM ja WHERE!
DELETE FROM
	student
WHERE
	id > 18

-- need olid andmete CRUD operations
--Create (Insert), Read (Select), Update, Delete
-- loo uus tabel loan, millel on väljad: amount(reaalarv), start_date, due_date,
-- student_id,   
CREATE TABLE loan(
	id serial PRIMARY KEY,
	amount numeric(11, 2) NOT NULL,
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL 	
);

--lisa neljale laenud, lisaks kahele neis lisa veel 1 laen
INSERT INTO loan
	(amount, start_date, due_date, student_id)
VALUES
	(12000, '01.01.2000', '31.12.2030', 7),
	(25000, '15.03.2010', '14.02.2040', 13),
	(120500, '01.12.2000', '30.11.2050', 13),
	(18500, '10.08.1999', '09.08.2039', 12),
	(2000, now(), '15.05.2042', 1),
	(5000, '01.11.2015', '30.10.2030', 7);
	
--anna kõik õpilased koos oma laenudega
SELECT 
	* 
FROM 
	student
JOIN
	loan
	ON student.id = loan.student_id
--student tabeli id on võrdne loan tabeli student_id-ga
--anna kõikide laenanud õpilaste ees- ja perekonnanimi koos oma laenu summa ja lõpukuupäevaga
SELECT 
--student.first_name, last_name, loan.amount, due_date -toimib ka nii, kui pole korduvaid välju
	student.first_name, 
	student.last_name,
	loan.amount,
	loan.due_date 
FROM 
	student
JOIN
	loan
	ON student.id = loan.student_id
-- INNER JOIN on vaikimisi join, INNER sõna võib ära jätta, selline tabelite liitmine, kus liidetakse
-- ainult need read, kus on võrdsed student.id =loan.student_id ehk need read, kua tabelite vahel on seos,
--ülejäänuid ignoreeritakse

	
--aga ainult suuremad kui 10000,
--järjesta laenu summa järgi
SELECT 
	student.first_name,
	student.last_name,
	loan.amount,
	loan.due_date 
FROM 
	student
JOIN
	loan
	ON student.id = loan.student_id
WHERE
	loan.amount > 10000
ORDER BY
	loan.amount DESC

--loo uus tabel loan_type, milles väljad name ja description
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);

--uue välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int 	

--tabeli kustutamine
DROP Table student;

--lisasime kirjeldused
INSERT INTO loan_type
	(name, description)
VALUES
	('Õppelaen', 'See on väga hea laen'),
	('SMS-laen', 'See on väga halb laen'),
	('Väikelaen', 'See on väga kõrge intressiga laen'),
	('Kodulaen', 'See on hea intressiga laen')
	
--anna õpilaste ees- ja perekonnanimi ning laenusumma koos laenutüübiga tabelist student, mis on liidetud tabeliga loan
-- läbi student_id, mis omakorda on seotud loan_type tabeliga läbi loan_type id
SELECT
	s.first_name, s.last_name, l.amount, lt.name	
FROM
	student AS s
JOIN
	loan AS l
	ON s.id =l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
--INNER JOIN korral pole joinimise järjekord oluline
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan_type AS lt
JOIN
	loan AS l
	ON lt.id =l.loan_type_id
JOIN
	student AS s
	ON s.id = l.student_id

--	LEFT JOIN puhul võetakse joini esimesest(VASAK) tabelist kõik read ja teises(PAREM) tabelis
--näidatakse puuduvad veerud/väärtused nullina

SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id =l.student_id
	
--kolme tabeli ühendamisel on oluline järjekord
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
--anna kõik kombinatsioonid kahe tabeli vahel, välistades iseendaga
SELECT
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s.first_name != st.first_name
	
--annab mõlemast kõik välistades need read, kus pole vastet
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id

--anna mingi kõigi kasutajate nimed, kes võtsid SMS-laenu ja kelle laenu kogus on üle 100 euro. tulemused järjesta
--laenu võtja vanuse järgi väiksemast suuremaks
SELECT
	s.first_name, s.last_name, l.amount, lt.name	
FROM
	student AS s
JOIN
	loan AS l
	ON s.id =l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE
	lt.name = 'SMS-laen'
	AND	l.amount > 100
ORDER BY
	s.birthday DESC
	
--keskmise leidmine, jäetakse välja read, kus väärtus on null/puudub
SELECT
	AVG(height)
FROM
	student
	
--
-- kui palju on 1 õpilane keskmiselt laenu võtnud, coalesce(loan.amount, 0)-määrab neil, kes pole 
--laenu võtnud, laenusummaks 0
SELECT 
	student.first_name,
	student.last_name,
	COALESCE(loan.amount, 0)
		 
FROM 
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
--Aggregate functions
--Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses küsida
--mingit muud välja tabelist, sest agregaatfunktsiooni tulemus on alati ainult 1 number ja 
--seda ei saa kuidagi näidata koos väljadega, mida võib olla mitu rida
SELECT 
	
	AVG (COALESCE (loan.amount, 0))
		 
FROM 
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
--see näitab keskmist, koos nendega, kes pole laenu võtnud (loan.amount, 0)

http://127.0.0.1:57993/browser/

--komakohtade määramiseks ROUND (millest, mitu kohta peale koma)
SELECT 
	
	ROUND(AVG (COALESCE (loan.amount, 0)),2) AS "keskmine laenusumma",
	MIN(loan.amount)AS "minimaalne laenusumma",
	MAX(loan.amount)AS "maksimaalne laenusumma"
		 
FROM 
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
--COUNT- loendab kokku (sulgudes) etteantud väärtuse, (*) tähendab kõiki veerge
SELECT 
	
	ROUND(AVG (COALESCE (loan.amount, 0)),2) AS "keskmine laenusumma",
	MIN(loan.amount)AS "minimaalne laenusumma",
	MAX(loan.amount)AS "maksimaalne laenusumma",
	COUNT(*) AS "Kõikide ridade arv",
	COUNT (loan.amount) AS "Laenude arv",--jäetakse välja read, kus väärtus on NULL
	COUNT(student.height)--näitab, mitmel tabeli "student" real on väärtus "height"	 
		 
FROM 
	student
LEFT JOIN
	loan
	
--grupeerimine
--kasutades GROUP BY jäävad SELECT välja jaoks alles vaid need väljad, mis on GROUP BY-s ära toodud
--antud juhul s.first_name, s.last_name, teisi välju saab ainult kasutada agregaatfunktsioonide sees
SELECT
	s.first_name,
	s.last_name,
	SUM(l.amount),
	ROUND(AVG(l.amount),2),
	MIN(l.amount),
	MAX(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name
	

--anna laenude summad sünniaastate järgi
SELECT
	date_part('year',s.birthday),
	SUM(l.amount)
FROM	
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
	
GROUP BY
	date_part('year',s.birthday)
	
--anna laenude summad laenutüüpide järgi
SELECT
	lt.name, 
	SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	lt.name

--laenu summad gr õpilase ning laenu tüübi kohta
SELECT
	s.first_name,
	s.last_name,
	lt.name, 
	SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name
ORDER BY
	s.last_name
	
--anna laenude, mis ületavad 10000, summad sünniaastate järgi, lisa eesnimi ja reasta need sünnipäeva järgi
SELECT
	date_part('year', s.birthday),
	s.first_name,
	SUM(l.amount)
FROM	
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday), s.first_name
--HAVING on nagu WHERE aga peale GROUP BY kasutamist
--filtreerimisel saab kasutada ainult neid välju, mis on GROUP BY lauses
--lisaks agregaatfunktsioone
HAVING
	date_part('year', s.birthday) IS NOT NULL--välistab need read, kus õpilastel read olid tühjad 
	AND	SUM(l.amount) > 10000--anna ainult need, kus summa suurem kui
	AND COUNT(l.amount) = 2 -- anna ainult need read, kus laene oli kokku 2
ORDER BY
	date_part('year', s.birthday)
	

--anna mitu laenu mingist tüübist on võetud ja mis on laenude summad	
SELECT
	lt.name,
	COUNT (l.id),
	SUM(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	 lt.name
	 
--mis aastal sündinud on võtnud kõige rohkem laenu
SELECT
	date_part('year',s.birthday),
	s.first_name,
	s.last_name,
	
	SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	date_part('year',s.birthday), s.first_name, s.last_name
ORDER BY
	SUM(l.amount)DESC
	
--anna õpilaste eesnime esitähe esinemise statistika e siis mitme õpilase eesnimi algab mingi tähega	
--substring, mis sõnast, mitmendast tähest  ja mitu tähte
SELECT 
	SUBSTRING (first_name, 1, 1), COUNT(SUBSTRING(first_name, 1, 1)) 
FROM
	student AS s
GROUP BY
	SUBSTRING(first_name, 1, 1)
	
--nested query, üks SELECT on teise SELECTi sees
--anna õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT
	first_name, last_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student)

--nested query, üks SELECT on teise SELECTi sees
--anna õpilased, kelle eesnimi on keskmise pikkusega õpilaste keskmine nimi
SELECT
	first_name, last_name
FROM
	student
WHERE
 	first_name IN
	
(SELECT
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student))
	
--tabelist teise tabelisse andmete lisamine/tõstmine, INSERT ja SELECT muutujad peavad olema samad
--lisa kaks vanimat õpilast töötajate tabelisse
INSERT INTO employee(first_name, last_name, birthday, middle_name)

SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday LIMIT 2

--kui tahame õpilasi seostada tundidega, vaja teha vahetabel mõlema tabeli nimega
--teisendamine täisarvuks
SELECT CAST(ROUND(AVG(age)) AS UNSIGNED)
 From emp99

	 	 --tõsta terve tabel korraga, parem klahv-Send to SQL editor-create statement- kirjuta andmebaasinimi
		 --
-- järjestab 3 pilti värskemast alates
SELECT * FROM accounts.pictures order by timeAdded desc;

-- järjestab albumid uuemast
SELECT * FROM accounts.albums order by id desc;		 