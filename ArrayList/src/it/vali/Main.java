package it.vali;

import jdk.swing.interop.SwingInterOpUtils;

import javax.imageio.metadata.IIOMetadataFormatImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	    int[] numbers = new int[5];

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        //lisa massiivi 5 numbrit
        numbers[0] = -4;
        numbers[1] = 2;
        numbers[2] = 3;
        numbers[3] = 2;

        // eemalda siit teine number
        numbers[1] = 0;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        numbers[4] = numbers[2];
        numbers[2] = numbers[0];
        numbers[0] = numbers[4];

        System.out.println();
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        //et saada massiivist andmeid ja neid seal töödelda, lihtsam kasutada ArrayListi
        // tavalisse ArrayListi võime lisada ükskõik mis tüüpi elemente
        // elemente küsides peab teadma, mis tüüpi kus täpselt asub ja peab selleks tüübiks teisendama/ cast -ima
        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();
        list.add(money);
        list.add(random);

        //tahame esimest numbrit sellest listist ja esimest sõna
        int a = (int)list.get(1);
        String word = (String)list.get(0);

        int sum = 3 + (int)list.get(1);

        if(Integer.class.isInstance(a)) {
            System.out.println("a on int");
        }
        if(Integer.class.isInstance(list.get(1))) {
            System.out.println("listis indeksiga 1 on int");
        }
        if(String.class.isInstance(list.get(0))) {
            System.out.println("listis indeksiga 0 on string");
        }

        //saab lisada kindlasse kohta vahele
        list.add(2, "head päeva ");

        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        //lisada
        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(2.343);

        // saab lisada ühe listi teise sisse
        list.addAll(otherList);
       // list.addAll(3, otherList); //kindlasse kohta

        System.out.println();
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        if(list.contains(money)) {
            System.out.println("money asub listis");
        }

        System.out.printf("money asub listis asukohaga %d%n", list.indexOf(money));

        System.out.printf("23 asub listis asukohaga %d%n", list.indexOf(23));

        System.out.println(list.size());
        //asendab list.set()
        //String sentence = list.get(0) + " hommikust";

        //System.out.println(sum);




    }
}
