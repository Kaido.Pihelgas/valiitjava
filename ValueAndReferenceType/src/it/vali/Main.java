package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // kui panna üks primitiiv (value type) tüüpi muutuja võrduma teist muutujaga
        // siis tegelikult tehakse arvuti mällu uus muutuja ja väärtus kopeeritakse sinna

        //sama juhtub ka primitiiv tüüpi muutuja kaasa andmisel meetodi parameetriks
        //(tegelikult tehakse arvuti mällu uus muutuja ja väärtus kopeeritakse sinna)

	    int a = 3;
	    int b = a;
	    a = 7;

        System.out.println(b);

        increment(b);
        System.out.println(b);


        //kui panna reference type/ viit tüüpi muutuja võrduma teise muutujaga
        //siis tegelikult jääb mälus ikkagi alles ainult üks muutuja, luihtsalt teine muutuja
        //hakkab viitama samale kohale mälus(samale muutujale)
        //meil on kaks muutujat aga tegelikult nad on täpselt sama objekt
        //sama juhtub ka reference type/ viit tüüpi muutuja kaasa andmisel
        //

        int[] numbers = new int[] { -5};

        int[] secondNumbers = numbers;
        numbers[0] = 3;

        System.out.println(secondNumbers[0]);

        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 3;

        Point pointB = pointA;
        pointB.x = 7;

        System.out.println(pointA.x);

        List<Point> points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);

        pointA.y = -4;

        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);

        pointA.printNotStatic();

        Point.printStatic();

        Point pointC = new Point();
        pointC.x = 12;
        pointC.y = 20;
        increment(pointC);
        System.out.println(pointC.x);
        System.out.println();

        int[] thirdNumbers = new int[] {1, 2, 3};
        increment(thirdNumbers);
        System.out.println(thirdNumbers[0]);


        Point pointD = new Point();
        pointD.x = 5;
        pointD.y = 6;
        pointD.increment();
        System.out.println(pointD.y);

//        String word = "Tere";
//        //peale.punkti on konkreetse Stringi meetodid, mis käivad selle enda kohta
//        word.length();
//        //peale.punkti on üldiselt Stringi meetodid
//        String.join();
    }

    public static void increment(int a) {
        a++;
        System.out.printf("a on nüüd %d%n", a);
    }
    // suurendame x ja y koordinaate 1 võrra
    //static= sa ei pea objekti looma, et meetodit välja kutsuda
    //mitte staatilise objekti jaoks peab looma objekti
    public static void increment(Point point) {
        point.x++;
        point.y++;
        System.out.printf("Uued koordinaadid on %d %d%n", point.x, point.y);
    }
    //suurendame kõiki elemente 1 võrra
    public static void increment(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;

        }

    }



}
