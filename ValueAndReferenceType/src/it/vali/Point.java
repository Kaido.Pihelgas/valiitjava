package it.vali;

public class Point {
    public int x;
    public int y;

    static void printStatic() {
        System.out.println("Olen staatiline punkt.");
    }

    public void printNotStatic() {
        System.out.println("Olen punkt.");
    }
    public void increment() {
        x++;
        y++;
    }
}
