package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// küsi külastaja nime
        //kui nimi on listis, sis öeldakse kasutajale "Tere tulemas, lll!"
        //ja küsitakse vanust
        //kui kasutaja on alaealine, print: Sorry, sisse ei pääse
        // muul juhul print Tere tulemast klubisse


        //kui kasutaja ei ole listis, küsi perekonnanime
        // kui see on listis, siis öeldakse Tere tulemast "ees ja perekonnanimi" klubisse
        // muul juhul "ma ei tunne sind"
        Scanner scanner = new Scanner(System.in);
        String savedFirstName = "Kaido";
        String savedLastName = "Pihelgas";

        System.out.println("Tere, mis on Sinu nimi?");
        String questFirstName = scanner.nextLine();
        // kõik väikesteks tähtedeks .toLowerCase
        //kui nimi on listis, sis öeldakse kasutajale "Tere tulemas, lll!"
        if (savedFirstName.toLowerCase().equals(questFirstName.toLowerCase())) {
            System.out.printf("Tere tulemast %s%n", questFirstName);
            //ja küsitakse vanust
            System.out.println("Mis on Sinu vanus?");

            //kui kasutaja on alaealine, print: Sorry, sisse ei pääse
            // muul juhul print Tere tulemast klubisse
            int vanus = Integer.parseInt(scanner.nextLine());
            if (vanus >= 18) {
                System.out.println("Tere tulemast klubisse! " + questFirstName);
            } else {
                System.out.println("Sorry, sisse ei pääse!");

            }
        }
        //kui kasutaja ei ole listis, küsi perekonnanime
        else
             {
                        System.out.println("Mis on Sinu perekonnanimi?");
                        String questLastName = scanner.nextLine();
                 // kui see on listis, siis öeldakse Tere tulemast klubisse
                        if (savedLastName.toUpperCase().equals(questLastName.toUpperCase())) {
                            System.out.printf("Tere tulemast %s sugulane!%n", savedFirstName);

                        }
                        // muul juhul "ma ei tunne sind"
                        else {
                            System.out.println("Ma ei tunne Sind!");
                        }


                    }

        }
    }

