package it.vali;
// public -tähendab, et klass meetod või muutuja on avalikult nähtav/ ligipääsetav,
// main - meetod, mis alustab programmi,
// class - javas üksus, eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld- klassi nimi, mis on ka faili nimi,
// static - meetodi ees, et seda meetodit saab välja kutsuda ilma klassist objekti loomata,
// void - meetod ei tagasta midagi,
// 	meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse, eraldadades komaga,
// String[]- tähitab Stringi massiivi,
// arghs - massiivi nimi, mis sisaldab käsurealt kaasa pandud parameetreid,
// System.out.println on java meetod, millega saab välja printida rida teksti,
//
public class Main {

    public static void main(String[] args) {
	System.out.println("Hello World!");
    }
}
