package it.vali;

import java.io.*;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
	// loe failist Input.txt iga teine rida ning kirjuta need read faili output.txt

        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane" +
                    "\\Documents\\input.txt", Charset.forName("Cp1252"));//Notepad vaikimisi salvestab ANSI dekodeerimist
            //Selleks, et Filereader oskaks seda korralikult lugeda(täpitähed), peab ette määrama, et loe ANSI formeeringus
            // Cp1252 on java-s ANSI formaat
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            FileWriter fileWriter = new FileWriter("c:\\users\\opilane" +
                    "\\Documents\\output.txt", Charset.forName("UTF-8"));//vaikimisi on javas UTF-8, ei pea määratlema
            // võib määratleda ka Cp1252 aga parem UTF-8
            int readCount = 1;

            String line = bufferedReader.readLine();
            while (line != null) {


                if(readCount %2 == 1) {
                fileWriter.write(line + System.lineSeparator());
                }

                line = bufferedReader.readLine();
                readCount += 1;

            }
            bufferedReader.close();
            fileReader.close();
            fileWriter.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

