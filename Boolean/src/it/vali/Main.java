package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // booleani muutujanimi iseloomustab väärtust
        boolean isMinor = false;

        System.out.println("Kui vana Sa oled?");

        Scanner scanner = new Scanner(System.in);

        int age = Integer.parseInt(scanner.nextLine());

        if (age < 18) {
            isMinor = true;
        }

        if (isMinor) {
            System.out.println("Oled alaealine");
        } else {
            System.out.println("Oled täisealine");
        }
        // võib kasutada if-blokkide asemel booleani

        int number = 10;

        boolean numberIsGreaterThan3 = number > 3;

        // || või puhul piisab ühest õigest vastusest, et kogu lahend oleks tõsi
        if (number > 2 && number < 6 || number > 10 || number < 20 || number == 100) {
        }

        boolean a = number > 2;
        boolean b = number < 6;
        boolean c = number > 10;
        boolean d = number < 20;
        boolean e = number == 100;
        boolean f = a && b;

        if (f || c || d || e) {
        }
        // küsi, kas on söönud hommikusööki
        System.out.println("Kas Sa hommikusööki sõid? Kirjuta jah/ei");
        boolean hasEaten = false;

        String answer = scanner.nextLine();

        if (answer.equals("jah")) {
            hasEaten = true;
        }
        // küsi, kas on söönud lõunat
        System.out.println("Kas Sa lõunat sõid? Kirjuta jah/ei");
        // String ette pole vaja, kuna juba on määratletud
        answer = scanner.nextLine();

        if (answer.equals("jah")) {
            hasEaten = true;
        }
        // küsi, kas on söönud õhtusööki
        System.out.println("Kas Sa õhtusööki sõid? Kirjuta jah/ei");
        answer = scanner.nextLine();

        if (answer.equals("jah")) {
            hasEaten = true;
        }
        // lõpuks kontrollib hasEaten staatust
        if (hasEaten) {

            System.out.println("Sa oled täna söönud.");
        }
        // prindi, kas on  täna söönud
        else {
            System.out.println("Sa pole täna söönud.");

        }
    }
}
