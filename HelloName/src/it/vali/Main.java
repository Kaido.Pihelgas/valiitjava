package it.vali;

public class Main {

    public static void main(String[] args) {
        // Deklareerime  tekstitüüpi (String) muutuja (variable)
        // mille nimeks name ja väärtuseks Kaido
        String name = "Kaido";	// write your code here
        String pereNimi = "Pihelgas";
        System.out.println("Hello " + name + " " + pereNimi + "!");
    }
}
