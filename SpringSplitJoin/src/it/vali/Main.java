package it.vali;

public class Main {

    public static void main(String[] args) {
	//
        String [] words = new String[] {"Põdral", "maja", "metsa", "sees" };

        for (int i = 0; i <words.length ; i++) {
            System.out.println(words[i]);
        }
        System.out.println();
        // prindi kõik sõnad massiivist, mis algavad m-tähega

//        for (int i = 0; i <words.length ; i++) {
//            if(words[i].startsWith("m")) {
//                System.out.println(words[i]);
//            }
//        }
        //
        for (int i = 0; i <words.length ; i++) {
            String firstLetter = words[i].substring(0,1);

            if(firstLetter.toLowerCase().equals("m")){
                System.out.println(words[i]);
             }


         }


        System.out.println();
        //kõik a- tähega lõppevad sõnad
        for (int i = 0; i <words.length ; i++) {
            if(words[i].endsWith("a"))
            {
            System.out.println(words[i]);
            }

        }
        //teine võimalus:
//        for (int i = 0; i <words.length ; i++) {
//            //otsib sõna kogu massiivist
//            String lastLetter = words[i].substring(words[i].length() - 1);
//
//            if(lastLetter.toLowerCase().equals("a")){
//                System.out.println(words[i]);
//            }
//
//
//        }

        System.out.println();
        // loe üle kõik sõnad, mis sisaldavad a-tähte
        for (int i = 0; i <words.length ; i++) {
            if(words[i].contains("a"))
            // võib kasutada ka indexOf-i
            {
                System.out.println(words[i]);
            }

        }
        System.out.println();

        // kõik sõnad, kus on 4 tähte
        for (int i = 0; i <words.length ; i++) {
            int lettersInWord = words[i].length();
            if(lettersInWord == 4)
            {
                System.out.println(words[i]);
            }

         }
        System.out.println();


        // kõige pikem sõna
        String longestWord = words[0];

        for (int i = 1; i <words.length ; i++) {

            if(words[i].length() > longestWord.length()){
            longestWord = words[i];

                System.out.println("Pikim sõna on " + words[i]);
            }

        }
        System.out.println();

        //algab ja lõpeb ühe tähega
        for (int i = 0; i <words.length ; i++) {
            // substring(0, 1) on sõna algus ja substring(words[i].length() - 1) on sõna lõpp
            if(words[i].substring(0, 1).equals(words[i].substring(words[i].length() - 1))) {
                System.out.println(words[i]);

//            if (words[i].charAt(0)== words[i].charAt(words.length -1)) {
//                System.out.println(words[i]);
            }
        }
    }
}

