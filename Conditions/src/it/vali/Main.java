package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

// java on Type-safe language(tüübikindel keel)
        if(a == 3) {
            System.out.printf("Arv %d on võrdne kolmega%n", a);
        }
        if (a != 4) {
            System.out.printf("Arv %d ei võrdu neljaga%n", a);
        }
        if (a < 5) {
            System.out.printf("Arv %d on väiksem viiest%n", a);
        }
        if (a >=6) {
            System.out.printf("Arv %d on suurem või võrdne kuuega%n", a);
        }
        if (!(a >= 7)) {
            System.out.printf("Arv %d ei ole suurem ega võrdne seitsmega%n", a);
        }

        // kui vaja 2 tingimust täita
       // arv on 2 ja 8 vahel
        // kui tingimuste vahel on ja &&, siis esimese tingimuse täitumisel edasisi enam ei vaadata
        // kui kõikide vahel on või ||, siis esimese tingimuse täitumisel edasisi enam ei vaadata
        if( a > 2 && a < 8) {
           System.out.printf("Arv %d on suurem kui kaks ja väiksem kui kaheksa%n", a);
    }
        // arv on väiksem kui 2 või arv on suurem kui 8
        if (a < 2 || a > 8) {
            System.out.printf("Arv %d on väiksem kui kaks või suurem kui kaheksa%n", a);
        }
        // arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või atv on suutem kui 10
        // && ja tehakse esimesena || tehakse teisena, soovitatav kasutada ()
        if ((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10) {
            System.out.printf("Arv %d on kahe ja kaheksa vahel või arv on viie ja kaheksa vahel või arv on suurem kui kümme%n", a);
            //arv ei ole 4 ja 6 vahel aga on 5 ja 8 vahel
            if ((!(a > 4 && a < 6) && a > 5 && a < 8) || (a < 0 && !(a > -14))) {
                System.out.printf("Arv %d ei ole nelja ja kuue vahel aga on viie ja kaheksa vahel või arv on negatiivne aga pole suurem kui neliteist%n", a);
            }
            //arv on negatiivne aga pole suurem kui 14
        }

}
}

