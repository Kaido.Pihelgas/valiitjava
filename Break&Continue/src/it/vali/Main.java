package it.vali;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // küsime kasutajalt pin koosi, kui see on õige, ütleme "Tore"
        //muul juhul küsime uuesti aga kokku 3 korda
        Scanner scanner = new Scanner(System.in);


        String pinKood = "1234";
        for (int i = 0; i < 3; i++) {

            System.out.println("Sisesta PIN kood");
            String valiPin = scanner.nextLine();

            if (valiPin.equals(pinKood)) {

                System.out.println("Tore! Õige PIN kood");
                break;
                // katkestab ja enam ei küsi nt teisel korral arvasid õigesti

            }

        }
        // prindi välja 10 kuni 20 ja 40 kuni 60
        //continue jätab selle tsükli korduse katki ja läheb järgmise tsükli juurde
        for (int i = 10; i <= 60; i++) {
            if(i > 20 && i < 40) {
                continue;
            }
            System.out.println(i);
        }
    }

}

        // while tsükliga raskem
//        int retriesLeft = 3;
//
//        do {
//            System.out.println("Sisesta PIN kood");
//            retriesLeft--;
//
//        } while (!scanner.nextLine().equals(pinKood) && retriesLeft > 0);
//
//        if () {
//            System.out.println("Panid kolm korda valesti");
//        }else {
//            System.out.println("Tore! Õige PIN kood");
//        }
//         }


