package it.vali;
// import- antud klassile MAIN lisatakse ligipääs java class Library paketile java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // loome Scanner tüübist objekti scanner, mille kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner(System.in);
        System.out.println("Tere, mis on Sinu nimi?");
        String name = scanner.nextLine();

        System.out.println("Tere " + name + "!");
        System.out.println("Mis on Sinu perenimi?");
        String lastName = scanner.nextLine();
        System.out.println("Mis on Sinu lemmikvärv?");
        String color = scanner.nextLine();
        System.out.println("Mis autoga Sa sõidad?");
        String car = scanner.nextLine();
        System.out.println("Tere " + name + " " + lastName + "!");
        System.out.println("Sulle meeldib " + color + " värv ja Sa sõidad " + car + "!");
        System.out.printf("Tere %s %s!\nSulle meeldib %s värv ja Sa sõidad %s!\n",
                name, lastName, color,car);
        StringBuilder builder = new StringBuilder();
        builder.append("Tere ");
        builder.append(name + " ");
        builder.append(lastName + "!\n");
        builder.append("Sulle meeldib ");
        builder.append(color);
        builder.append(" värv ja Sa sõidad ");
        builder.append(car + "!\n");

        builder.toString();
        System.out.printf(builder.toString());
        String text = String.format("Tere %s %s! \nSulle meeldib %s värv ja Sa sõidad %s!",
                name, lastName, color,car);
        //Nii System.out.println kui kaString.format kasutavad enda siseseslt StringBuilderit
        System.out.println(text);

    }
}
