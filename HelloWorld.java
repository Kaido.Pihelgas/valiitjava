// public -tähendab, et klass meetod või muutuja on avalikult nähtav/ ligipääsetav,
// main - meetod, mis alustab programmi,
// class - javas üksus, eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld- klassi nimi, mis on ka faili nimi,
// static - meetodi ees, et seda meetodit saab välja kutsuda ilma klassist objekti loomata,
// void - meetod ei tagasta midagi,
// 	meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse, eraldadades komaga,
// String[]- tähitab Stringi massiivi,
// arghs - massiivi nimi, mis sisaldab käsurealt kaasa pandud parameetreid,
// System.out.println on java meetod, millega saab välja printida rida teksti,
//
public class HelloWorld {
	public static void main(String[] arghs) {
		System.out.println("Hello World!");
		System.out.println("tere kevad!");
		System.out.println("paistab, et programm töötab. öötöö on möödas!");
		System.out.println("Käes on suvi.");
	} 
}