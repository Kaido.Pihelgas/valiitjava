package it.vali;

public class Main {

    public static void main(String[] args) {
	// casting on teisendamine arvutüübist teise tüüpi
        byte a = 3;
        // kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine
        // ingl k-s implicit casting
        short b = a;
        // kui üks nuymbritüüp ei pruugi teise sisse mahtuda, siis peab kõigepealt ise veenduma,
        // et see number mahub sinna ja kasutama teisenduseks()
        // ingl k-s explicit casting
        short c = 300;
        byte d = (byte)c;
        // kuna ei mahu (byte suurus on 256 ühikut), teeb ringi täis ja siis jääb seisma tulemil 44
        System.out.println(d);

        long e = 10000000000000L;
        int f = (int) e;
        System.out.println(f);
        // väiksemast suuremasse saab panna
        long g = f;
        g = c;

        float h = 123.23424F;
        double i = h;


        double j = 55.1111111111111;
        float k = (float) j;
        // kui teisendame väiksemasse ( () abil), siis toimub ümardamine, kaotame väärtuses
        System.out.println(k);

        double l = 12E50; // 12 * 10astmes50
        float m = (float) l;

        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;
        System.out.println(q);

        int r = 2;
        int s = 9;
        // double * int = double, int / int = int,
        // double / int = double, double + int = double
        System.out.println(r / (double)s);
        System.out.println(r / (float)s);

        // double / float = double
        float t = 12.55555F;
        double u = 23.555555;
        System.out.println(t/u);





    }
}
