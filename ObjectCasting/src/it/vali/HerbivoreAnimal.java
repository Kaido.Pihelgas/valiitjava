package it.vali;

public class HerbivoreAnimal extends WildAnimal {
    public boolean isEatVegetables() {
        return eatVegetables;
    }

    public void setEatVegetables(boolean eatVegetables) {
        this.eatVegetables = eatVegetables;
    }

    public boolean eatVegetables = true;


}
