package it.vali;

public class Fox extends CarnivoreAnimal {
    public boolean isHasLongTail() {
        return hasLongTail;
    }

    public void setHasLongTail(boolean hasLongTail) {
        this.hasLongTail = hasLongTail;
    }

    public boolean hasLongTail = true;

}
