package it.vali;

public class Giraffe extends HerbivoreAnimal {
    public boolean isEatsTreeLeaves() {
        return eatsTreeLeaves;
    }

    public void setEatsTreeLeaves(boolean eatsTreeLeaves) {
        this.eatsTreeLeaves = eatsTreeLeaves;
    }

    public boolean eatsTreeLeaves = true;

}
