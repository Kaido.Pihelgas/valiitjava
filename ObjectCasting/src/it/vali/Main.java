package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	int a = 100;
	short b = (short) a;
    a = b;


	// iga kassi võib võtta kui looma
        // implicit casting
	Animal animal = new Cat();

	// iga loom ei ole kass, vaja explicit cast-ida

       Cat cat = (Cat)animal;

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        dog.setName("Naki");
        Fox fox = new Fox();
        Lion lion = new Lion();


        animals.add(dog);
        dog.setName("Muki");
        animals.add(cat);
        animals.add(lion);
        //saab viimasele lisada erinevaid väärtusi
        animals.get(animals.size()-1).setWeight(150);
        animals.add(new Cow());
        //teisendada
        ((Cow)animals.get(animals.size()-1)).setFarmerName("Juhan");
        animals.add(new HerbivoreAnimal());

        fox.setBreed("Hõberebane");

        //saab otse lisada listi erinevaid parameetreid
        animals.get(animals.indexOf(dog)).setName("Pontu");



        Giraffe giraffe = new Giraffe();
        Antilopes antilopes = new Antilopes();

        //kutsu kõikide loomade printinfo välja
        //foreach- iga element, mis tüüpi, nimi on koheselt määratletud, siis : listi nimi, millest otsib
        for (Animal animalInList: animals) {
            animalInList.printInfo();
            System.out.println();
        }
        for (Animal animalInList: animals) {
            animalInList.eat();
        }

        Animal secondAnimal = new Dog();
        //kohe ei saa koera omadusi, vaja teisendada (Dog) ja kogu nimi uuesti sulgudesse
        ((Dog)secondAnimal).setHasTail(true);
        secondAnimal.printInfo();



    }
}
