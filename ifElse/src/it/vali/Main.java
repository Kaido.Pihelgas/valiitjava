package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	//kui arv on 4, siis prindi arv on 4
        // muul juhul kui arv on negatiivne siis kontrolli, kas arv on suurem kui -10, prindi sellekohane teksr
        // muul juhul kontrolli, kas arv on suurem kui 20, prindi tulem
        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta üks number");
        int a = Integer.parseInt(scanner.nextLine());


        if (a == 4) {
            System.out.println("Arv on neli");

        }
        else {
            if (a < 0) {
                if (a > -10)
                    System.out.println("Arv on negatiivne ja suurem kui -10");
            }
            if (a > 20) {
            System.out.println("Arv on suurem kui 20");
        }
    }
}
}

