package it.vali;

import com.sun.source.tree.WhileLoopTree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
	//
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");
        //bufferedreader võimaldab lugeda faili rea kaupa
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();

            //kui readLine avastab, et järgmist rida tegelikult ei ole, tagastab see meetod null
            while (line != null) {
                System.out.println(line);
                // kui panna LINE võrduma bufferedline-ga, loeb iga kord järgmise rea
                line = bufferedReader.readLine();

            }



            bufferedReader.close();
            fileReader.close();

            //catchiga saab kasutada korraga mitut veapüüdjat
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
