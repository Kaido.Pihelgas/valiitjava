package it.vali;

public class Motorbike extends Vehicle implements Driver, DriverInTwoWheels {
    private int maxDistance;
    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Mootorratas sõidab");

    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Mootorratas peatus peale %d km järel%n", afterDistance);
    }

    @Override
    public void driveInRearWheel() {
        System.out.println("Mootorratas sõidab tagumisel rattal");
    }
}
