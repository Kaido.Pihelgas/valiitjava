package it.vali;

//interface/liides sunnib seda kasutavat/implementeerivat klassi omama liideses kirja pandud meetodeid
//(sama tagastustüübiga ja sama parameetrite kombinatsiooniga)
// iga klass, mis interface kasutab, määrab ise ära meetodi sisu
public interface Flyer {
    void fly();

}
