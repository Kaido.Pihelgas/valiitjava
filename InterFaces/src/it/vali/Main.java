package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	//saab 2 täiesti erinevad objekti hoida samas liideses
        Flyer bird = new Bird();
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();
        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);
        System.out.println();

        for (Flyer flyer: flyers) {
            flyer.fly();
            System.out.println();

        }
        Car car = new Car();
        Driver plane1 = new Plane();
        //Driver car = new Car();
        Driver aeroflot = new Plane();
        Driver harley = new Motorbike();


        List<Driver> drivers = new ArrayList<Driver>();
        Driver audi = new Car();
        drivers.add(aeroflot);
        drivers.add(harley);
        drivers.add(audi);
        drivers.add(car);
        drivers.add(plane1);
        drivers.add((Driver) plane);
        ((Driver) plane).drive();
        car.drive();
        audi.stopDriving(10);
        harley.drive();
        harley.getMaxDistance();
        harley.stopDriving(55);
        audi.getMaxDistance();


        car.setMake("Audi");
        System.out.println("Tere".toString());
        System.out.println(boeing);


        //lisa liides Driver, klass Car.
        // Mõtle kas lennuk ja auto võiks mõlemad kasutada Driver liidest?
        //Driver liides võiks sisaldada 3 meetodi kirjeldust
        //int getMAaxDistance
        // void drive()
        // void stopDriving(int afterDistance)- saad määrata, mitme meetri pärast lõpetab sõitmise
        //pane auto ja lennuk mõlemad kasutama seda liidest
        // lisa mootorratas
    }
}
