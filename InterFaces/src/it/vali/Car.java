package it.vali;

public class Car extends Vehicle implements Driver {
    private int maxDistance;

    public Car() {

    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    private String make;

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    //loon konstruktori, et saaks igale autole määrata maxdistance
    public Car(int maxDistance) {
        this.maxDistance = maxDistance;

    }



    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Auto sõidab maanteel");

    }

    @Override
    public void stopDriving(int afterDistance) {

        System.out.printf("Auto lõpetas sõitmise %d km järel.%n", afterDistance);

    }

    @Override
    public String toString() {
        return make;
    }
}
