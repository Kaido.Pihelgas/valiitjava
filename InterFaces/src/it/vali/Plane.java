package it.vali;

//Plane/lennuk pärineb klassist Vehicle/sõiduk ja kasutab/implementeerib liidest Flyer/lendaja
public class Plane extends Vehicle implements Flyer, Driver {
    private int maxDistance;
    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Lennuk lendab");
    }

    private void doCheckList() {
        System.out.println("Täidetakse checklist");
    }

    private void startEngine() {
        System.out.println("Mootor on käivitunud");
    }


    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Lennuk ruleerib õhkutõusu rajale");

    }

    @Override
    public void stopDriving(int afterDistance) {

        System.out.println("Lennuk peatub terminali juures");

    }
}
