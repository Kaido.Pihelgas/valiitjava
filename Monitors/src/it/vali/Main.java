package it.vali;

import java.rmi.MarshalledObject;

public class Main {

    public static void main(String[] args) {
	    Monitor firstMonitor = new Monitor();
	    Monitor secondMonitor = new Monitor();

	    firstMonitor.manufacturer = "Philips";
	    firstMonitor.color = Color.WHITE;
	    firstMonitor.diagonal = 27;
	    firstMonitor.screenType = ScreenType.AMOLED;

	    secondMonitor.manufacturer = "LG";
	    secondMonitor.color = Color.BLACK;
	    secondMonitor.diagonal = 24;
	    secondMonitor.screenType = ScreenType.LCD;


        System.out.println(secondMonitor.screenType);
        System.out.println(firstMonitor.manufacturer);

        firstMonitor.color = Color.BLACK;

        Monitor[] monitors = new Monitor[4];

        //lisa kolmas monitor
        //prindi kõikide tootja, mille diagonaal on suurem kui 25 tolli

        Monitor thirdMonitor = new Monitor();
        thirdMonitor.manufacturer = "Samsung";
        thirdMonitor.color = Color.WHITE;
        thirdMonitor.diagonal = 27;
        thirdMonitor.screenType = ScreenType.OLED;

        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;
        monitors[3] = new Monitor();
        monitors[3].manufacturer = "Sony";
        monitors[3].color = Color.GREY;
        monitors[3].diagonal = 21;
        monitors[3].screenType = ScreenType.LCD;

        System.out.println();

        for (int i = 0; i < monitors.length ; i++) {
            if(monitors[i].diagonal > 25)
            System.out.println(monitors[i].manufacturer);

        }
        System.out.println();
        //leia monitori värv kõige suuremal monitoril
        Monitor maxSizeMonitor = monitors[0];


        for (int i = 0; i < monitors.length ; i++) {

            if(monitors[i].diagonal > maxSizeMonitor.diagonal) {
                maxSizeMonitor = monitors[i];
            }
            System.out.println("Suurima diagonaaliga on " + maxSizeMonitor.manufacturer);
           // System.out.println(maxSizeMonitor.manufacturer);
            // System.out.println(maxSizeMonitor.color);
            maxSizeMonitor.printInfo();
        }

        System.out.printf("Monitori diagonaal cm-tes on %.2f%n", maxSizeMonitor.diagonalToCm());


    }





}
