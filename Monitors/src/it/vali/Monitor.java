package it.vali;
//enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid,
// salvestatakse int-na, saab seostada a la String/double/int
//nt BLACK = 1, WHITE = 2 jne
enum Color {
        BLACK, WHITE, GREY
    }
enum ScreenType {
    LCD, TFT, OLED, AMOLED
    }

public class Monitor {
    private String manufacturer;
    private double diagonal; //tollides "

    public String getManufacturer() {
        return manufacturer;
    }

    public double getDiagonal() {
        return diagonal;
    }

    public it.vali.Color getColor() {
        return color;
    }

    public it.vali.ScreenType getScreenType() {
        return screenType;
    }

    private Color color;
    private ScreenType screenType;

    void printInfo() {
        System.out.println();
        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraanitüüp %s%n", screenType);
        System.out.println();
    }

    // tee meetod, mis tagastab ekraani diagonaali cm
    double diagonalToCm() {
        return diagonal * 2.54;

    }
}
