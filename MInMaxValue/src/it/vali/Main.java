package it.vali;

public class Main {

    public static void main(String[] args) {
	// leia suurim element
        int[] numbers = new int[] { -2, 4, 11, 12, 0, 161 };
        int max = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if(numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println(max);

        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);
        // leia suurim paarituarv, mitmes nr nimekirjas on
        // määratleme täisarvu miinimumi, selleks käsk
        max = Integer.MIN_VALUE;
        int jrk = 1;
        boolean oddNumbersFound = false;
        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                jrk = i + 1;
                oddNumbersFound = true;
            }
        }if(oddNumbersFound) {
            System.out.println("Suurim paaritu arv on " + max + " ja see asub reas kohal " + jrk + ".");
        }else {
            System.out.println("Paaritud arvud puuduvad.");
        }

        //teine võimalus läbi muutuja jrk/position saame kontrollida, kas üldse on paarituid
        max = Integer.MIN_VALUE;

        int position = -1;

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;

            }
        }if(position != -1) {
            //System.out.println("Suurim paaritu arv on " + max + " ja see asub reas kohal " + jrk + ".");
            System.out.printf("Suurim paaritu arv on %d ja see asub reas kohal %d.", max, jrk);
        }else {
            System.out.println("Paaritud arvud puuduvad.");
        }
        }

        //System.out.printf("Suurim paaritu arv on %d ja see asub reas kohal %d.", max, jrk);
        // kui paaritu arve pole, prindi" paaritud arvud puuduvad"


}
