package it.vali;

public class Main {

    public static void main(String[] args) {
	// Massiiv/järjend/nimekiri
       //saad ühes muutujas hoida mitu elementi
        // massiivinimi võiks olla mitmuses ja lõpus mitu elementi hoiab
        // loomise hetkel määratud elementida arvu hiljem muuta ei saa

       int [] numbers = new int [5];


       //massiivi indeksid algavad nullist mitte 1-st
        // viimane indeks on alati väiksem kui massiivi pikkus

        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;

        System.out.println(numbers[3]);

        System.out.println();

        // i asendab indeksi pesanumbrit
        for (int i = 0; i < 5; i++) {
            System.out.println(numbers[i]);

        }
        System.out.println();
            //prindi tagurpidi
        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);

        }
        //suuremad kui 2
        System.out.println();
        for (int i = 0; i < numbers.length; i++) {
            if( numbers[i] > 2){

            System.out.println(numbers[i]);}

        }
        // paarisarvud
        System.out.println();
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {

            System.out.println(numbers[i]);}

        }
        // tagantpoolt 2 esimest paaritut arvu
        System.out.println();
        int counter = 0;
        for (int i = 4; i >= 0; i--) {
            if(numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if(counter == 2){
                    break;
                }
            }

            System.out.println(numbers[i]);

        }
        System.out.println();
        int [] newNumbers = new int [3];

        newNumbers[0] = numbers[0];
        newNumbers[1] = numbers[1];
        newNumbers[2] = numbers[2];

        for (int i = 0; i < newNumbers.length; i++) {
            newNumbers[i] = numbers[i];
        }
        for (int i = 0; i < newNumbers.length ; i++) {

           System.out.println(newNumbers[i]);
        }

        System.out.println();
        // loo kolmas massiiv ja pane sinna esimesest tagantpool kolm

//        newNumbers[0] = numbers[4];
//        newNumbers[1] = numbers[3];
//        newNumbers[2] = numbers[2];

        for (int i = 0; i < newNumbers.length ; i++) {
            newNumbers[i] = numbers[numbers.length -i -1];

        }

        for (int i = 0; i < newNumbers.length ; i++){
        System.out.println(newNumbers[i]);
    }
        System.out.println();
       int[] thirdNumbers = new int[3];
        // eelmisega sama, lisame uue muutuja tsüklisse komaga
        // lisame komaga tegevuse uue massiivi jaoks
        for (int i = 0, j = numbers.length-1; i < thirdNumbers.length; i++, j-= 2) {

            thirdNumbers[i] = numbers[j];

        }
        for (int i = 0; i < thirdNumbers.length ; i++){
            System.out.println(thirdNumbers[i]);
        }

}
}

